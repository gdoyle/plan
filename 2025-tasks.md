## Weekly assigned tasks - 2025 📝

This file is meant to track and communicate my priorities and tasks on a weekly basis. I use my design tracking issues for issue and OKR statuses (ex: gitlab-org/gitlab#345031), which should be the SSOT of what design issues are being taken on for Runner Fleet for a specific milestone.

## March 🥶🌥️🌱

### Mar 3 - Mar 7

**Highest priorities**
- Updating vision for GH integration based on feedback
- Creating MVC design for GH integration

<details><summary>Previous months</summary>

## February 🥶🌨️⛄️

### Feb 24 - Feb 28

**Highest priorities**
- Vision for GitHub Integration
- Collaborating on the PRD

### Feb 18 - Feb 21 (2/17 public holiday)

**Highest priorities**
- Vision for GitHub Integration
- Collaborating on the PRD

### Feb 10 - Feb 15

**Highest priorities**
- Sharing External CI Integration research insights in https://gitlab.com/gitlab-org/ux-research/-/issues/3324 and with stakeholders
- Starting on workflow designs

**Don't forget**
- Create issues for log improvements that were discussed here: https://gitlab.com/gitlab-org/gitlab/-/issues/510717#note_2330082179

### Feb 3 - Feb 7

**Highest priorities**
- Reviewing and anaylzing research for External CI Job project

**Don't forget**
- Create issues for log improvements that were discussed here: https://gitlab.com/gitlab-org/gitlab/-/issues/510717#note_2330082179

## January ❄️☃️❅

### Jan 30 - Jan 31 (OOO sick time early in week)

**Highest priorities**
- Planning 17.9, 17.10+ design issues for PE borrow
- Meeting PE teammates
- Getting caught up on External CI Job project
- Reviewing and anaylzing research for External CI Job project

### Jan 21 - Jan 23

**Highest priorities**
- Review Package MRs
- PE borrow?

### Jan 13 - Jan 17

**Highest priorities**
- Review Package MRs
- Plan 17.9 and 17.10 milestones
- Start up on Runner issues

### Jan 6 - Jan 10

**Highest priorities**
- Catch up from PTO 
- Review Package MRs

**Don't forget**
- If there's an MR review, ask Jesse to join a sync call

</details>