## Weekly assigned tasks - 2022 📝

This file is meant to track and communicate my priorities and tasks on a weekly basis. I use my design tracking issues for issue and OKR statuses (ex: https://gitlab.com/gitlab-org/gitlab/-/issues/345031), which should be the SSOT of what design issues are being taken on for Runner and Testing for a specific milestone.

----

<details>
<summary markdown="span"> 🚀 Sometime in the near future 🚀 </summary>

- [ ] Open enrollment for 2023 benefits, update FSA to $150/pay period
- [ ] Add https://youtube.com/playlist?list=PL05JrBw4t0Kpj9T7ZNk2u4vGfRLSkgZis to Testing group page
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/
- [ ] Create an MR to update "maturity" column on JTBD tables
- [ ] Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues
- [ ] Update Build Artifacts user stories based on research https://gitlab.com/gitlab-org/ux-research/-/issues/1956

</details>

----

### December 🎅❄️🎄

----

### December 19th - December 22nd (OOO Dec 23 - Jan 2 - [Coverage issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2172))

**Highest priorities**
- Tufts sponsor project issue
- Qualtrics survey for feedback on artifacts page

✅ Completed ✅
- [x] Async source-a-thon
- [x] Fill out 15.7 retro
- [x] Share Tufts info with the team
- [x] Update Tufts issue with new details and timeline 
- [x] Update qualtrics survey based on feedback

↻ In progress ↻
- [ ] Assigned projects issue
- [ ] Flaky tests MVC - add jobs info https://gitlab.com/gitlab-org/gitlab/-/issues/381799/designs/mr-widget.png
- [ ] Create test plan for flaky tests 
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Create MR to update ways Runner team works with design issues
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues

❌ Not started ❌
- [ ] Create an issue to discuss customer office hours and tag RV

### December 12th - December 16th

**Highest priorities**
- Tufts sponsor project issue
- Qualtrics survey for feedback on artifacts page
- Iterations for runner queues

✅ Completed ✅
- [x] Share Tufts sponsor info with RV
- [x] you can ask the research team about it in the issue / slack channel
- [x] Create an issue for async feedback on Tufts sponsorship: include the details, what we’ll need in terms of resources, and timeline. We’ll need approval from Legal. We’ll also need to figure out who else needs to approve it. Define a high lvl plan of what the project is about, who would be involved other than you.
- [x] Create qualtrics survey
- [x] Leave a comment on [project runner redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/33803/) to run research first and tag RV
- [x] MVC for runner queues (with modal)
- [x] Update survey based on feedback

↻ In progress ↻
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Create MR to update ways Runner team works with design issues
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues

❌ Not started ❌
- [ ] Create an issue to discuss customer office hours and tag RV

### December 5th - December 9th

**Highest priorities**
- Create qualtrics survey for feedback on artifacts page
- MVC for flaky tests
- MVC for runner queues

✅ Completed ✅
- [x] Artifacts feedback survey in google doc
- [x] MVC for flaky tests

↻ In progress ↻
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Create MR to update ways Runner team works with design issues
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Leave a comment on [project runner redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/33803/) to run research first and tag RV

❌ Not started ❌
- [ ] Create an issue to discuss customer office hours and tag RV

### November 28 - December 2nd

**Highest priorities**
- Complete Pr doc
- Create qualtrics survey for feedback on artifacts page

✅ Completed ✅
- [x] Fill our pr doc and ping RV
- [x] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [x] Scope down [project runner redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/33803/) and leave a comment for RV
- [x] Due Nov 26th: [UX retro issue](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/62)

↻ In progress ↻
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Create MR to update ways Runner team works with design issues
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Leave a comment on [project runner redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/33803/) to run research first and tag RV

❌ Not started ❌
- [ ] Create an issue to discuss customer office hours and tag RV

----

### November 🍁🍂🌞

----

### November 22 (000 Nov 21st, 23rd-25th)

**Highest priorities**
- Prep for pr. doc
- Prep for 15.7 milestone work

✅ Completed ✅
- [x] Create pr. doc and share with RV

↻ In progress ↻
- [ ] Create MR to update ways Runner team works with design issues
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues

❌ Not started ❌
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Create an issue to discuss customer office hours and tag RV
- [ ] Scope down [project runner redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/33803/) and leave a comment for RV
- [ ] Due Nov 26th: [UX retro issue](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/62)

### November 14 - November 16 (000 Nov 17th, F&F day Nov 18th)

**Highest priorities**
- Add flaky test summary insights to https://gitlab.com/gitlab-org/gitlab/-/issues/3673
- Create qualtrics survey for https://gitlab.com/gitlab-org/ux-research/-/issues/2202

↻ In progress ↻
- [ ] Create MR to update ways Runner team works with design issues
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues

❌ Not started ❌
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Create an issue to discuss customer office hours and tag RV
- [ ] Scope down [project runner redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/33803/) and leave a comment for RV
- [ ] Due Nov 26th: [UX retro issue](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/62)

### November 7 - November 10 (Short week with Veteran's Day)

**Highest priorities**
- Create future milestone planning issues
- Complete 15.6 design issues
- Redesign https://gitlab.com/gitlab-org/gitlab/-/issues/360826+

✅ Completed ✅
- [x] Create 15.7 planning issue
- [x] Create 15.8 planning issue
- [x] Create 15.9 planning issue
- [x] https://gitlab.com/gitlab-org/gitlab/-/issues/376717+ proposal
- [x] https://gitlab.com/gitlab-org/ux-research/-/issues/2202+ draft of survey

↻ In progress ↻
- [ ] Create MR to update ways Runner team works with design issues
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues

❌ Not started ❌
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Create an issue to discuss customer office hours and tag RV
- [ ] Scope down [project runner redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/33803/) and leave a comment for RV
- [ ] Due Nov 26th: [UX retro issue](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/62)

### October 31 - November 4

**Highest priorities**
- Catching up from OOO time
- Create future milestone planning issues

✅ Completed ✅
- [x] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/371621 into issues
- [x] Health benefits updates for 2023
- [x] Expense report for KubeCon

↻ In progress ↻
- [ ] Create 15.7 planning issue
- [ ] Create 15.8 planning issue

❌ Not started ❌
- [ ] Create MR to update ways Runner team works with design issues
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Create an issue to discuss customer office hours and tag RV
- [ ] Scope down [project runner redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/33803/) and leave a comment for RV
- [ ] Due Nov 26th: [UX retro issue](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/62)

----

### October 🍁🍂🌞

----

### October 24 - October 28

OOO for KubeCon NA

### October 17 - October 21

**Highest priorities**
- Finish [designs in feedback issues](https://gitlab.com/gitlab-org/gitlab/-/issues/378161#-design-issues-in-feedback-land-44-limit-4)
- Create future planning issues

✅ Completed ✅
- [x] Create 15.6 planning issue
- [x] Finish filling out [Coverage issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2133)
- [x] Complete proposal for https://gitlab.com/gitlab-org/gitlab/-/issues/343652+
- [x] Complete 15.5 milestone retro
- [x] Share proposal for https://gitlab.com/gitlab-org/gitlab/-/issues/365479+
- [x] Due Oct 14th: [Summary of performance in workday](https://docs.google.com/document/d/1QdfJjGmbIQL1fTJlT1jAK3BpqjxG7ivy4y4emhR1I4A/edit#)

↻ In progress ↻
- [ ] Create 15.7 planning issue
- [ ] Create 15.8 planning issue
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/371621 into issues

❌ Not started ❌
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Create an issue to discuss customer office hours and tag RV
- [ ] Scope down [project runner redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/33803/) and leave a comment for RV

### October 11 - October 14 (short week with holiday)

**Highest priorities**
- Design for https://gitlab.com/gitlab-org/gitlab/-/issues/365479+
- Review MRs and catch up from OOO
- Personal performance assessment for FY23

✅ Completed ✅
- [x] Due Oct 14th: [Performance assessment google doc](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2122)
- [x] Share design for https://gitlab.com/gitlab-org/gitlab/-/issues/365479+
- [x] Create Coverage issue for Kubecon week

↻ In progress ↻
- [ ] Create 15.6 - 15.8 planning issues
- [ ] Finish filling out [Coverage issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2133)
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/371621 into issues
- [ ] Due Oct 14th: [Summary of performance in workday](https://docs.google.com/document/d/1QdfJjGmbIQL1fTJlT1jAK3BpqjxG7ivy4y4emhR1I4A/edit#)

❌ Not started ❌
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Create an issue to discuss customer office hours and tag RV
- [ ] Scope down [project runner redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/33803/) and leave a comment for RV

### October 3 - October 6 (short week with F&F day)

**Highest priorities**
- Record results of Runner Fleet CMS and share
- MVC Runner CI job queue updates

✅ Completed ✅
- [x] Create issue for discoverability of review apps experiment - tag EB
- [x] Create and send out CMS walkthrough
- [x] Finalize job queue MVC
- [x] Go through 1:1 docs to see issues you need to create
- [x] Go through team meeting docs to see issues you need to create

↻ In progress ↻
- [ ] Create issue to update group swicth to enable stale runner cleanup to match admin text
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/371621 into issues
- [ ] Create an issue to discuss customer office hours and tag RV
- [ ] Scope down [project runner redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/33803/) and leave a comment for RV 

❌ Not started ❌
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues

----

### September 🍁🍂🌞

----

### September 27 - September 30 (short week with F&F day)

**Highest priorities**
- Analyze CMS sessions for Runner Fleet
- Walk through results and share ^

↻ In progress ↻
- [ ] Create issue to update group swich to enable stale runner cleanup to match admin text
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/371621 into issues
- [ ] Create an issue to discuss customer office hours and tag RV
- [ ] Create issue for discoverability of review apps experiment - tag EB
- [ ] Go through 1:1 docs to see issues you need to create
- [ ] Go through team meeting docs to see issues you need to create
- [ ] Scope down [project runner redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/33803/) and leave a comment for RV 

❌ Not started ❌
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues

### September 23 (really short week with unexpected PTO)

**Highest priorities**
- Analyze CMS sessions for Runner Fleet
- Walk through results and share ^

✅ Completed ✅
- [x] Due Sept 18th: https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/59

↻ In progress ↻
- [ ] Create an issue to discuss customer office hours and tag RV
- [ ] Create issue for discoverability of review apps experiment - tag EB
- [ ] Go through 1:1 docs to see issues you need to create
- [ ] Go through team meeting docs to see issues you need to create
- [ ] Scope down [project runner redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/33803/) and leave a comment for RV 

❌ Not started ❌
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues

### September 12 - September 14 (short week with PTO)

**Highest priorities**
- Analyze CMS sessions for Runner Fleet
- Review app location issue

✅ Completed ✅
- [x] Leave a comment in S2 KR that https://gitlab.com/gitlab-org/gitlab/-/merge_requests/94816 is merged but behind a FF

↻ In progress ↻
- [ ] Create an issue to discuss customer office hours
- [ ] Create issue for discoverability of review apps experiment - tag EB
- [ ] Go through 1:1 docs to see issues you need to create
- [ ] Go through team meeting docs to see issues you need to create
- [ ] Update https://gitlab.com/gitlab-org/gitlab/-/issues/33803/ and leave comment for RV 
- [ ] Ask in the open about Pajamas updates to pagination spacing - does that count?

❌ Not started ❌
- [ ] Scope down project runner search issue
- [ ] Pull https://gitlab.com/gitlab-org/gitlab/-/issues/33804 into 15.4
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Due Sept 18th: https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/59


### September 6 - September 9 (short week with holiday on Monday)

**Highest priorities**
- Run last research session for Runner Fleet CMS
- Analyze data from research sessions for review apps
- 15.4 issues

✅ Completed ✅
- [x] Create issue for https://gitlab.slack.com/archives/CPANF553J/p1661959703086369?thread_ts=1661445330.432419&cid=CPANF553J
- [x] Create issue for https://gitlab.slack.com/archives/CL9STLJ06/p1661959903513219?thread_ts=1661866547.937259&cid=CL9STLJ06
- [x] Expense Airbnb for meetup
- [x] Transfer https://docs.google.com/document/d/1zuMVj_EEK6RC7p4GndmmKNfzkrXiMAXXHCsQxUDlob0/edit to issues
- [x] Create issue for making label "in parantheses" more discoverable when filtering issues
- [x] Talk to Nadia + Erika about secrets and how we can use what's been done for KubeCon
- [x] Create wireframes for group runner issue

↻ In progress ↻
- [ ] Create an issue to discuss customer office hours
- [ ] Create issue for discoverability of review apps experiment - tag EB
- [ ] Go through 1:1 docs to see issues you need to create
- [ ] Go through team meeting docs to see issues you need to create
- [ ] Update https://gitlab.com/gitlab-org/gitlab/-/issues/33803/ and leave comment for RV 
- [ ] Ask in the open about Pajamas updates to pagination spacing - does that count?

❌ Not started ❌
- [ ] Scope down project runner search issue
- [ ] Leave a comment in S2 KR that https://gitlab.com/gitlab-org/gitlab/-/merge_requests/94816 is merged but behind a FF
- [ ] Pull https://gitlab.com/gitlab-org/gitlab/-/issues/33804 into 15.4
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Due Sept 18th: https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/59

----

### August 🏖🏐👙

----

### August 30 - September 2 (short week with F&F on Monday)

**Highest priorities**
- Support EF on KubeCon design activity
- Run last research session for Runner Fleet CMS
- Analyze data from research sessions for review apps
- 15.4 issues

↻ In progress ↻
- [ ] Create issue for https://gitlab.slack.com/archives/CPANF553J/p1661959703086369?thread_ts=1661445330.432419&cid=CPANF553J
- [ ] Create issue for https://gitlab.slack.com/archives/CL9STLJ06/p1661959903513219?thread_ts=1661866547.937259&cid=CL9STLJ06
- [ ] Create an issue to discuss customer office hours
- [ ] Create issue for discoverability of review apps experiment - tag EB
- [ ] Go through 1:1 docs to see issues you need to create
- [ ] Go through team meeting docs to see issues you need to create

- [ ] Update https://gitlab.com/gitlab-org/gitlab/-/issues/33803/ and leave comment for RV 
- [ ] Ask in the open about Pajamas updates to pagination spacing - does that count?
- [ ] Expense Airbnb for meetup

❌ Not started ❌
- [ ] Transfer https://docs.google.com/document/d/1zuMVj_EEK6RC7p4GndmmKNfzkrXiMAXXHCsQxUDlob0/edit to issues
- [ ] Create issue for making label "in parantheses" more discoverable when filtering issues
- [ ] Create wireframes for group runner issue
- [ ] Scope down project runner search issue
- [ ] Talk to Nadia + Erika about secrets and how we can use what's been done for KubeCon
- [ ] Leave a comment in S2 KR that https://gitlab.com/gitlab-org/gitlab/-/merge_requests/94816 is merged but behind a FF
- [ ] Pull https://gitlab.com/gitlab-org/gitlab/-/issues/33804 into 15.4
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Due Sept 18th: https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/59


### August 22 - August 26

**Highest priorities**
- Run research sessions for CMS runner fleet & review apps
- Summarize review app sessions so far in research issue
- 15.4 issues

✅ Completed ✅
- [x] 🚨 Due Aug 24: Send CICD meeting recording!!!!!!!!!
- [x] Update career development issue to include EB onboarding and JTBD research
- [x] Due July 29th: Create planning issues for 15.5+
- [x] Review https://gitlab.com/gitlab-org/gitlab/-/issues/346272 and come up with a plan for experimenting with https://gitlab.com/gitlab-org/gitlab/-/issues/343133
- [x] Put CICD meeting under me (Aug 24)
- [x] Send slack about CICD meeting agenda
- [x] Fill out https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/58
- [x] Book flights for KubeCon

↻ In progress ↻
- [ ] Update https://gitlab.com/gitlab-org/gitlab/-/issues/33803/ and leave comment for RV 
- [ ] Create issue for discoverability of review apps experiment - tag EB
- [ ] Ask in the open about Pajamas updates to pagination spacing - does that count?
- [ ] Create an issue to discuss customer office hours
- [ ] Expense Airbnb for meetup

❌ Not started ❌
- [ ] Transfer https://docs.google.com/document/d/1zuMVj_EEK6RC7p4GndmmKNfzkrXiMAXXHCsQxUDlob0/edit to issues
- [ ] 🚨 Due Aug 24: Send CICD meeting recording!!!!!!!!!
- [ ] Create issue for making label "in parantheses" more discoverable when filtering issues
- [ ] Create wireframes for group runner issue
- [ ] Scope down project runner search issue
- [ ] Talk to Nadia + Erika about secrets and how we can use what's been done for KubeCon
- [ ] Leave a comment in S2 KR that https://gitlab.com/gitlab-org/gitlab/-/merge_requests/94816 is merged but behind a FF
- [ ] Pull https://gitlab.com/gitlab-org/gitlab/-/issues/33804 into 15.4
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Due Sept 18th: https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/59


### August 15 - August 19

**Highest priorities**
- Run CMS sessions for runner fleet
- Summarize review app sessions so far in research issue

✅ Completed ✅
- [x] Due Aug 5th: Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/363056 to issues
- [x] Ping Rayana about KubeCon
- [x] All 360 review requests

↻ In progress ↻
- [ ] Update https://gitlab.com/gitlab-org/gitlab/-/issues/33803/ and leave comment for RV 
- [ ] Update career development issue to include EB onboarding and JTBD research
- [ ] Due July 29th: Create planning issues for 15.5+
- [ ] Review https://gitlab.com/gitlab-org/gitlab/-/issues/346272 and come up with a plan for experimenting with https://gitlab.com/gitlab-org/gitlab/-/issues/343133

❌ Not started ❌
- [ ] Expense Airbnb for meetup
- [ ] Create issue for discoverability of review apps experiment - tag EB
- [ ] Ask in the open about Pajamas updates to pagination spacing - does that count?
- [ ] Create an issue to discuss customer office hours
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Fill out https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/58

### August 8 - August 12 (Short week - OOO Thursday)

**Highest priorities**
- Prepping CMS environment for runner fleet
- Summarize review app sessions so far in research issue
- Breaking down https://gitlab.com/gitlab-org/gitlab/-/issues/368948

✅ Completed ✅
- [x] Due July 28th: Review https://gitlab.com/gitlab-org/gitlab/-/issues/365334#note_1037142385
- [x] Break down bulk actions work
- [x] Break down artifacts table work
- [x] Set up testing environment 

↻ In progress ↻
- [ ] Update https://gitlab.com/gitlab-org/gitlab/-/issues/33803/ and leave comment for RV 
- [ ] Update career development issue to include EB onboarding and JTBD research
- [ ] Due July 29th: Create planning issues for 15.5+
- [ ] Review https://gitlab.com/gitlab-org/gitlab/-/issues/346272 and come up with a plan for experimenting with https://gitlab.com/gitlab-org/gitlab/-/issues/343133

❌ Not started ❌
- [ ] Due Aug 5th: Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Due Aug 5th: Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Due Aug 5th: Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/363056 to issues
- [ ] Due Aug 18th: Fill out https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/58
- [ ] Due Aug 31st: All 360 review requests

### August 1 - August 5 (Short week - OOO Thursday)

**Highest priorities**
- Sending out emails for more participants for review app solution validation
- Run sessions for review app solution validation
- Summarize review app sessions so far in research issue
- Breaking down https://gitlab.com/gitlab-org/gitlab/-/issues/368948

✅ Completed ✅
- [x] Work with EB to come up with a plan for tracking instrumentation of the rollout of https://gitlab.com/gitlab-org/gitlab/-/issues/343133#note_1040711516
- [x] Prep for 1:1 on G&D with RV
- [x] Leave a Q2 retro in career development issue
- [x] Send out invites for https://gitlab.com/gitlab-org/ux-research/-/issues/1977

↻ In progress ↻
- [ ] Update https://gitlab.com/gitlab-org/gitlab/-/issues/33803/ and leave comment for RV 
- [ ] Update career development issue to include EB onboarding and JTBD research
- [ ] Due July 28th: Review https://gitlab.com/gitlab-org/gitlab/-/issues/365334#note_1037142385
- [ ] Due July 29th: Create planning issues for 15.5+

❌ Not started ❌
- [ ] Due Aug 2nd: Review https://gitlab.com/gitlab-org/gitlab/-/issues/346272 and come up with a plan for experimenting with https://gitlab.com/gitlab-org/gitlab/-/issues/343133
- [ ] Due Aug 5th: Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Due Aug 5th: Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Due Aug 5th: Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/363056 to issues
- [ ] Due Aug 18th: Fill out https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/58

----

### July 🏝⛱🐚☀️

----

### July 25 - July 29

**Highest priorities**
- Sending out emails for participants for Runner Fleet CMS
- Run sessions for review app solution validation

✅ Completed ✅
- [x] Complete design for updated list view for runners
- [x] Finish MR for pajamas
- [x] Proposal for project runners view (related to OKR)
- [x] Send out messages to people for 360 feedback
- [x] Select people for 360 feedback in cultureamp
- [x] Complete https://gitlab.com/gitlab-org/gitlab/-/issues/337838
- [x] Due July 30th: As feedback comes in about the UX roadmap, leave a comment on https://gitlab.com/gitlab-org/gitlab-design/-/issues/1954#note_1025789440 with how it went.
- [x] Finish tablet and mobile views for `creator` for runner list https://gitlab.com/gitlab-org/gitlab/-/issues/355767
- [x] Discuss with DE where list view for runners falls into planning https://gitlab.com/gitlab-org/gitlab/-/issues/343133/
- [x] Update interview shadow table

↻ In progress ↻
- [ ] Send out invites for https://gitlab.com/gitlab-org/ux-research/-/issues/1977
- [ ] Due July 28th: Review https://gitlab.com/gitlab-org/gitlab/-/issues/365334#note_1037142385
- [ ] Due July 29th: Create planning issues for 15.5+
- [ ] Work with EB to come up with a plan for tracking instrumentation of the rollout of https://gitlab.com/gitlab-org/gitlab/-/issues/343133#note_1040711516

❌ Not started ❌
- [ ] Due Aug 2nd: Review https://gitlab.com/gitlab-org/gitlab/-/issues/346272 and come up with a plan for experimenting with https://gitlab.com/gitlab-org/gitlab/-/issues/343133
- [ ] Due Aug 5th: Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Due Aug 5th: Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Due Aug 5th: Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/363056 to issues
- [ ] Due Aug 18th: Fill out https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/58

### July 18 - July 22

**Highest priorities**
- Sending out emails for participants for Runner Fleet CMS
- Finishing up OKR issues - https://gitlab.com/gitlab-org/gitlab/-/issues/337838

✅ Completed ✅
- [x] Due July 18th: Create actionable insights for https://gitlab.com/gitlab-org/ux-research/-/issues/1940
- [x] Due July 18th: Leave comments in retro issue https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/57
- [x] Due July 18th: Create actionable insights for https://gitlab.com/gitlab-org/ux-research/-/issues/1956
- [x] Due Jul 18th: Create an issue for https://docs.google.com/document/d/1QHlJhaWdAXxibYMxDASEoBiljJJB8aCaEL26bOkxfxo/edit#
- [x] Due July 19th: Responsive version of runner list view
- [x] Due July 21st: Send out invites for review app sessions

↻ In progress ↻
- [ ] Complete https://gitlab.com/gitlab-org/gitlab/-/issues/337838
- [ ] Due July 25th: Create planning issues for 15.5+
- [ ] Due Jul 25th: Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Due July 25th: Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Due July 25th: Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/363056 to issues

❌ Not started ❌
- [ ] Due July 30th: Update Build Artifacts user stories based on research https://gitlab.com/gitlab-org/ux-research/-/issues/1956
- [ ] Due Jul 30th: Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues
- [ ] Due July 30th: As feedback comes in about the UX roadmap, leave a comment on https://gitlab.com/gitlab-org/gitlab-design/-/issues/1954#note_1025789440 with how it went.
- [ ] Due July 30th: Create an MR to update "maturity" column on JTBD tables

### July 12 - July 15 (Short week with F&F day July 11)

**Highest priorities**
- Updating runner list view based on feedback 

✅ Completed ✅
- [x] Make updates to https://gitlab.com/gitlab-org/gitlab/-/issues/33804 based on feedback
- [x] Create UX roadmap async walkthrough
- [x] Post walkthrough in #UX, PI slack channel
- [x] Update milestone and KR issues to check off completed steps
- [x] Due July 14th: Leave comments for https://gitlab.com/gitlab-org/ux-research/-/issues/1875#note_1025427814
- [x] Due July 14th: Fill out 15.3 milestone issue for PI
- [x] Due July 14th: Fill out 15.3 milestone issue for Runner
- [x] Add summary of runner solution validation sessions to issue - unmoderated
- [x] Clean up runner versions issue #339523
- [x] Add summary of runner solution validation sessions to issue - moderated
- [x] Make updates to runner list view based on feedback

↻ In progress ↻
- [ ] Due July 18th: Responsive version of runner list view
- [ ] Due July 18th: Create actionable insights for https://gitlab.com/gitlab-org/ux-research/-/issues/1940
- [ ] Due July 18th: Update Build Artifacts JTBDs based on research
- [ ] Due July 18th: Create actionable insights for https://gitlab.com/gitlab-org/ux-research/-/issues/1956
- [ ] Due July 18th: Create planning issues for 15.5+

❌ Not started ❌
- [ ] Due Jul 18th: Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Due Jul 18th: Create an issue for https://docs.google.com/document/d/1QHlJhaWdAXxibYMxDASEoBiljJJB8aCaEL26bOkxfxo/edit#
- [ ] Due July 18th: Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Due July 18th: Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/363056 to issues
- [ ] Due July 18th: Leave comments in retro issue https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/57
- [ ] Due Jul 30th: Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues
- [ ] Due July 30th: As feedback comes in about the UX roadmap, leave a comment on https://gitlab.com/gitlab-org/gitlab-design/-/issues/1954#note_1025789440 with how it went.


### July 5 - July 8 (Short week with Public Holiday July 4)

**Highest priorities**
- Breaking down artifacts page for development https://gitlab.com/gitlab-org/gitlab/-/issues/33418/
- Analyzing moderated sessions for https://gitlab.com/gitlab-org/ux-research/-/issues/1940

✅ Completed ✅
- [x] Create discussion guide for https://gitlab.com/gitlab-org/ux-research/-/issues/1884 
- [x] Discuss with team about feasability of https://gitlab.com/gitlab-org/gitlab/-/issues/360447 proposal
- [x] Finish up artifacts page design
- [x] Review apps screener

↻ In progress ↻
- [ ] Make updates to https://gitlab.com/gitlab-org/gitlab/-/issues/33804 based on feedback

❌ Not started ❌
- [ ] Make updates to runner list view based on feedback
- [ ] Responsive version of runner list view
- [ ] Add summary of runner solution validation sessions to issue
- [ ] Planning issues for 15.5+
- [ ] Due July 18th: Leave comments in retro issue https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/57
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Create an issue for https://docs.google.com/document/d/1QHlJhaWdAXxibYMxDASEoBiljJJB8aCaEL26bOkxfxo/edit#
- [ ] Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues

### June 27 - July 1

**Highest priorities**
- Verify Hackathon week! 🎮
- Create discussion guide for https://gitlab.com/gitlab-org/gitlab/-/issues/357885
- Breaking down artifacts page for development https://gitlab.com/gitlab-org/gitlab/-/issues/33418/
- Analyzing moderated sessions for https://gitlab.com/gitlab-org/ux-research/-/issues/1940

✅ Completed ✅
- [x] Create UX roadmap issues by transferring stuff from Mural
- [x] Let CF know when screener is ready https://gitlab.com/gitlab-org/ux-research/-/issues/1977#note_974353494
- [x] Leave feedback on https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915#note_1007403785
- [x] Complete personal development assessment
- [x] Edits to artifacts page

↻ In progress ↻
- [ ] Responsive version of runner list view
- [ ] Add summary of runner solution validation sessions to issue
- [ ] Create discussion guide for https://gitlab.com/gitlab-org/gitlab/-/issues/357885

❌ Not started ❌
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Create an issue for https://docs.google.com/document/d/1QHlJhaWdAXxibYMxDASEoBiljJJB8aCaEL26bOkxfxo/edit#
- [ ] Due July 18th: Leave comments in retro issue https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/57
- [ ] Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues

----

### June 🌊☀️🕶🍉

----

### June 21 - June 23 (short week with OOO June 20 & 24)

**Highest priorities**
- Create discussion guide for https://gitlab.com/gitlab-org/gitlab/-/issues/357885
- Collecting feedback on artifacts page https://gitlab.com/gitlab-org/gitlab/-/issues/33418/
- Running moderated sessions for https://gitlab.com/gitlab-org/ux-research/-/issues/1940

✅ Completed ✅
- [x] Could you all link/add the studies that you’d like included in the Verify and Package Research Registry & Synthesis in this issue -> https://gitlab.com/gitlab-org/ux-research/-/issues/1738 
- [x] Incorporate feedback into artifacts page
- [x] Tag Pedro on Pipeline Insights issue
- [x] Finish UX roadmap exercise in Mural
- [x] Create feedback issue for review apps

↻ In progress ↻
- [ ] Create UX roadmap issues by transferring stuff from Mural

❌ Not started ❌
- [ ] Add summary of runner solution validation sessions to issue
- [ ] Create discussion guide for https://gitlab.com/gitlab-org/gitlab/-/issues/357885
- [ ] Create design placeholders for admin, group, and project views in https://gitlab.com/groups/gitlab-org/-/epics/7633
- [ ] Let CF know when screener is ready https://gitlab.com/gitlab-org/ux-research/-/issues/1977#note_974353494
- [ ] Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues
- [ ] Responsive version of runner list view

### June 13 - June 17

**Highest priorities**
- Making updates to artifacts page based on feedback from https://gitlab.com/gitlab-org/ux-research/-/issues/1859
- Analyzing UserTesting data for https://gitlab.com/gitlab-org/ux-research/-/issues/1940
- Running moderated sessions for https://gitlab.com/gitlab-org/ux-research/-/issues/1940

✅ Completed ✅
- [x] Create moderated discussion guide for https://gitlab.com/gitlab-org/ux-research/-/issues/1940
- [x] Review runner usertesting sessions
- [x] Due June 12th: https://gitlab.com/gitlab-org/gitlab/-/issues/341759#note_975814839
- [x] Reach out to VM about MVC for runner queues
- [x] Due June 18th: [Fill out 15.1 UX retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/56)
- [x] Open issue for https://gitlab.com/gitlab-org/gitlab/-/issues/348299#note_974099113
- [x] Open issue for https://gitlab.com/gitlab-org/gitlab/-/issues/343687#note_973791154
- [x] Share insights with PMdS to get input
- [x] Create actionable insight issues for artifacts
- [x] Map artifacts insights to research goals, questions, etc.
- [x] Make updates to artifacts page based on research
- [x] Close out 15.1 design issue
- [x] Update 15.2 design issue to include carryovers or waiting on feedback designs
- [x] [Project Runners: Add labels to unlabeled concepts and/or helper text to expand on Runner sub-concepts.](https://gitlab.com/gitlab-org/gitlab/-/issues/33804) design

↻ In progress ↻
- [ ] Could you all link/add the studies that you’d like included in the Verify and Package Research Registry & Synthesis in this issue -> https://gitlab.com/gitlab-org/ux-research/-/issues/1738 

❌ Not started ❌
- [ ] Let CF know when screener is ready https://gitlab.com/gitlab-org/ux-research/-/issues/1977#note_974353494
- [ ] Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues
- [ ] Responsive version of runner list view

### June 6 - June 10

**Highest priorities**
- Making updates to artifacts page based on feedback from https://gitlab.com/gitlab-org/ux-research/-/issues/1859
- Launch https://gitlab.com/gitlab-org/ux-research/-/issues/1940

✅ Completed ✅
- [x] Ask P to pilot the UserTesting test
- [x] Due June 7th: Leave feedback for P on https://docs.google.com/presentation/d/1FVk77cHxWyvzakHm_3g1nr9rvZz_ca7nu7nDm2JEMC4/edit?usp=drive-slack&ts=629e0f9b
- [x] Analyze dovetail data for artifacts
- [x] Due June 7th: Prep for UX showcase for build artifacts
- [x] Pull out insights for artifacts
- [x] Due June 9th: Prep G&D plan for 1:1
- [x] Due June 10th: Fill out EB's onboarding issue - Please go ahead and update the intro section with any missing information, assign it to yourself, and review the UX Buddy tasks. We’ll share this on June 13th

↻ In progress ↻
- [ ] Create actionable insight issues for artifacts
- [ ] Share insights with PMdS to get input
- [ ] Create moderated discussion guide for https://gitlab.com/gitlab-org/ux-research/-/issues/1940
- [ ] Review runner usertesting sessions

❌ Not started ❌
- [ ] Let CF know when screener is ready https://gitlab.com/gitlab-org/ux-research/-/issues/1977#note_974353494
- [ ] Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues
- [ ] Responsive version of runner list view
- [ ] Due June 12th: https://gitlab.com/gitlab-org/gitlab/-/issues/341759#note_975814839
- [ ] Due June 18th: [Fill out 15.1 UX retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/56)

### May 31 - June 3 (short week OOO May 30th)

**Highest priorities**
- Analyzing data for https://gitlab.com/gitlab-org/ux-research/-/issues/1859
- Respond to feedback on Unmoderated testing for https://gitlab.com/gitlab-org/ux-research/-/issues/1940

✅ Completed ✅
- [x] Artifacts JTBD survey recruiting and survey finalizing
- [x] Due June 1st: Get back to M about UX showcase June 8th
- [x] Prep for research sessions coming up
- [x] Create an MR to add edge case to UX suggested reviewer
- [x] Leave a comment in artifacts recruitment with participants that didn't show up
- [x] Draft UserTesting survey for https://gitlab.com/gitlab-org/ux-research/-/issues/1940
- [x] Tag researcher to review

↻ In progress ↻
- [ ] Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues
- [ ] Responsive version of runner list view

❌ Not started ❌
- [ ] Ask P to pilot the UserTesting test
- [ ] Due June 6th: Prep G&D plan for 1:1
- [ ] Due June 7th: Prep for UX showcase for build artifacts
- [ ] Due June 18th: [Fill out 15.1 UX retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/56)

----

### May 🌸🌞⚾️

----

### May 23 - May 26

**Highest priorities**
- Running sessions for https://gitlab.com/gitlab-org/ux-research/-/issues/1859
- Discussion guide for https://gitlab.com/gitlab-org/ux-research/-/issues/1940

✅ Completed ✅
- [x] Create issue for showing artifacts in pipeline graph
- [x] Upload all artifacts sessions to dovetail
- [x] Update artifacts recruitment issue and tag CF
- [x] Create CMS issue for artifacts

↻ In progress ↻
- [ ] Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues
- [ ] Responsive version of runner list view

❌ Not started ❌
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/

### May 16 - May 20

**Highest priorities**
- Running sessions for https://gitlab.com/gitlab-org/ux-research/-/issues/1859
- Scheduling more participants for https://gitlab.com/gitlab-org/ux-research/-/issues/1859

✅ Completed ✅
- [x] Create issue for frontend portion of purging runners -- not necessary MR will make a proposal
- [x] Create issue for new icon in SVG repo
- [x] Create the prototype for https://gitlab.com/gitlab-org/ux-research/-/issues/1859 to go with the discussion guide
- [x] Create 15.3 issue and add test summary research issue (potential), enabling CI features (potential)
- [x] May 26: Leave comments in [15.0 UX retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/54)
- [x] Send out blocking invite for UX roadmap workshop

↻ In progress ↻
- [ ] Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues
- [ ] Responsive version of runner list view

❌ Not started ❌
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/

### May 9 - May 13

OOO - sick

### May 2 - May 6

**Highest priorities**
- MR reviews/assigned to me 
- Prototype for https://gitlab.com/gitlab-org/ux-research/-/issues/1859
- Recruitment for https://gitlab.com/gitlab-org/ux-research/-/issues/1859

✅ Completed ✅
- [x] Create a smaller screener for artifact solution and comment on https://gitlab.com/gitlab-org/ux-research/-/issues/1891#note_930305064 and tag JP
- [x] Write up and share a summary for [Self-service catalog](https://gitlab.com/gitlab-org/ux-research/-/issues/1927). Leave a comment with expected timeline
- [x] Vision part 2 video walkthrough
- [x] Move over all Murals in GitLab to Gitlab and then delete GitLab workspace in Mural
- [x] Finalizing environment for review apps issue
- [x] Make sure calendly is realistic in the times offered
- [x] Transfer screener in doc to qualtrics and comment in recruitment issue https://docs.google.com/document/d/1dbRSaLqtD4xU4_8GdXbtfATfIKOZJv_RT-QA1D8Epu8/edit
- [x] Create solution validation issue for runner list view and leave a comment that it is blocked until verified
- [x] Write up notes in runner list view

↻ In progress ↻
- [ ] Create the prototype for https://gitlab.com/gitlab-org/ux-research/-/issues/1859 to go with the discussion guide

❌ Not started ❌
- [ ] Create issue for frontend portion of purging runners
- [ ] Create 15.3 issue and add test summary research issue (potential), enabling CI features (potential)
- [ ] Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues
- [ ] May 26: Leave comments in [15.0 UX retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/54)
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/

----

### April 🌼🐇🌷

----

### April 25 - April 29

**Highest priorities**
- MR reviews/assigned to me 
- Prep work for UX OKRs
- Applying feedback to https://gitlab.com/gitlab-org/gitlab/-/issues/343133/
- Prototype for https://gitlab.com/gitlab-org/ux-research/-/issues/1859

✅ Completed ✅
- [x] Due April 25: Open issue to discuss modals from MR widgets and add mr working group label
- [x] Due April 25: Open MR to add modal documentation in https://design.gitlab.com/regions/merge-request-reports/
- [x] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/85812
- [x] Review MR: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/85923
- [x] Finalize who to shadow for interviews - sent out Slack message
- [x] Finalize Pajamas MR widget MR
- [x] Due April 29th: Complete Step 1 of [UX Roadmap issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1953)
- [x] Post update on https://gitlab.com/gitlab-org/gitlab/-/issues/343133/
- [x] Create an issue around MR widget usage metrics instrumentation and tag Jocelyn
- [x] Figure out recruitment plan for https://gitlab.com/gitlab-org/ux-research/-/issues/1859

↻ In progress ↻
- [ ] Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues

❌ Not started ❌
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/

### April 19 - April 22 (short week with OOO April 18 and 20)

**Highest priorities**
- Updating [15.0 design issue](https://gitlab.com/gitlab-org/gitlab/-/issues/352037) and closing out [14.10 design issue](https://gitlab.com/gitlab-org/gitlab/-/issues/351456) and completed issues
- Identifying SUS issues for PI and Runner
- Responding to feedback on vision part 2 https://gitlab.com/gitlab-org/gitlab/-/issues/345594

✅ Completed ✅
- [x] Leave a comment for EF on https://gitlab.com/gitlab-org/ux-research/-/issues/1873 with what you want to learn, how much you want to be involved, and how many hours you can be involved.
- [x] Due April 19th: Timings for [career development Mural](https://app.mural.co/invitation/mural/gitlab2474/1646230185696?sender=u8acaa5f2dd7f92154f687467&key=c09ba7e6-10b0-4c15-bbf7-a28f1c89a36f) actionable items. Potentially connect with OKRs if they've been announced.
- [x] Due April 19th: PI retro https://gitlab.com/gl-retrospectives/verify-stage/pipeline-insights/-/issues/23#note_915327812
- [x] Due April 17th or 18th: [UX retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/52)
- [x] Take https://www.linkedin.com/learning/making-user-experience-happen-as-a-team/first-say-no?u=2255073

↻ In progress ↻
- [ ] Due April 21: Identify SUS issues to prioritize for Q2 in Pipeline Insights and Runner
- [ ] Due April 22: Open MR to add modal documentation in https://design.gitlab.com/regions/merge-request-reports/
- [ ] Due April 22: Open issue to discuss modals from MR widgets and add mr working group label
- [ ] Finalize who to shadow for interviews

❌ Not started ❌
- [ ] Create MR to update https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/#design-issues-and-ssot with using design weights for design issues
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/1

### April 11 - April 15

**Highest priorities**
- Vision part 2 https://gitlab.com/gitlab-org/gitlab/-/issues/345594
- Responding to feedback on https://gitlab.com/gitlab-org/gitlab/-/issues/352528
- Responding to feedback on [discussion guide](https://docs.google.com/document/d/1ZLMdbU8xyDeUjNM1f0dTuHb24MSiRHFMidwLFWB00yc/edit?usp=sharing) for https://gitlab.com/gitlab-org/ux-research/-/issues/1884

✅ Completed ✅
- [x] For April 8th: Interview training
- [x] Phase 2 of Runner Enterprise epic, Identify the job: What is it? How do we define it?
- [x] MR for updating runner JTBDs
- [x] Due April 13th: Add steps to how you will get to actionable items in [career development Mural](https://app.mural.co/invitation/mural/gitlab2474/1646230185696?sender=u8acaa5f2dd7f92154f687467&key=c09ba7e6-10b0-4c15-bbf7-a28f1c89a36f)
- [x] Discussion guide for https://gitlab.com/gitlab-org/ux-research/-/issues/1884
- [x] Design for https://gitlab.com/gitlab-org/gitlab/-/issues/352528

↻ In progress ↻
- [ ] Leave a comment for EF on https://gitlab.com/gitlab-org/ux-research/-/issues/1873 with what you want to learn, how much you want to be involved, and how many hours you can be involved.
- [ ] Due April 14: Identify SUS issues to prioritize for Q2 in Pipeline Insights and Runner
- [ ] Finalize who to shadow for interviews

❌ Not started ❌
- [ ] Due April 17th or 18th: [UX retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/52)
- [ ] Due April 19th: Timings for [career development Mural](https://app.mural.co/invitation/mural/gitlab2474/1646230185696?sender=u8acaa5f2dd7f92154f687467&key=c09ba7e6-10b0-4c15-bbf7-a28f1c89a36f) actionable items. Potentially connect with OKRs if they've been announced.
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/1

### April 4 - April 8

**Highest priorities**
- Catching up from OOO
- 14.10 design issues

✅ Completed ✅
- [x] Update GL status
- [x] Due April 4th: Add measurable metrics to actionable steps in [career development Mural](https://app.mural.co/invitation/mural/gitlab2474/1646230185696?sender=u8acaa5f2dd7f92154f687467&key=c09ba7e6-10b0-4c15-bbf7-a28f1c89a36f)
- [x] Watch lightning demos for hypothesis generation workshop
- [x] Fill out 1:1 doc with RV
- [x] Action items for 1:1 with RV
- [x] Prep 1:1 doc with JP
- [x] Prep 1:1 doc with DE
- [x] Due April 6th: Ask MR and PP about OKRs
- [x] Due April 8th: Vote on idea groups for MR restructure workshop in [Mural](https://app.mural.co/t/gitlab2474/m/gitlab2474/1647336992246/276cdf1c92474f8da3621212a2e4cfb0e07b19b1)
- [x] Edit [pajamas MR](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2759#note_895015477) with feedback

↻ In progress ↻
- [ ] Design for https://gitlab.com/gitlab-org/gitlab/-/issues/352528
- [ ] MR for updating runner JTBDs
- [ ] For April 8th: Interview training

❌ Not started ❌
- [ ] Due April 13th: Add steps to how you will get to actionable items in [career development Mural](https://app.mural.co/invitation/mural/gitlab2474/1646230185696?sender=u8acaa5f2dd7f92154f687467&key=c09ba7e6-10b0-4c15-bbf7-a28f1c89a36f)
- [ ] Due April 17th or 18th: [UX retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/52)
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Read into https://gitlab.com/groups/gitlab-org/-/epics/7327 for performance testing
- [ ] Watch https://www.youtube.com/watch?v=EP3G-kjMDMs for performance testing
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/1

----

### March 🌱🌞🌻

----

### March 28 - April 1 : 🌴 OOO

### March 21 - March 24 (Short week with F&F day March 25th)

**Highest priorities**
- [Review MRs assigned to Gina](https://gitlab.com/dashboard/merge_requests?reviewer_username=gdoyle)
- Complete interview training
- 14.10 design issues

✅ Completed ✅
- [x] Due March 21: Fill out 3 and 4 with specific examples in [Mural](https://app.mural.co/invitation/mural/gitlab2474/1646230185696?sender=u8acaa5f2dd7f92154f687467&key=c09ba7e6-10b0-4c15-bbf7-a28f1c89a36f). Think of projects that are in front of you. For example, Pajamas - what would you have to do to contribute more or practice more within your stage group work?
- [x] Code review hypothesis workshop: Part 1 [prep work](https://app.mural.co/t/gitlab2474/m/gitlab2474/1647542388664/e3a3c3d7273561cdb60ac02207adbbe2d4bf7c5d?sender=u8acaa5f2dd7f92154f687467)
- [x] Due March 23: Add items to [cICD agenda ](https://docs.google.com/document/d/1fCMY77lzlsjHaCeqEY6pIw_K4bS_JYlfYlLJpPPJfKQ/edit#heading=h.38k2hxc90h8k)
- [x] Due March 25: Part 2 [prep work] for [hypothesis generation workshop](https://gitlab.com/gitlab-org/gitlab/-/issues/355468)

↻ In progress ↻
- [ ] Interview training
- [ ] Design for https://gitlab.com/gitlab-org/gitlab/-/issues/352528

❌ Not started ❌
- [ ] Due April 4th: Add measurable metrics to actionable steps in [career development Mural](https://app.mural.co/invitation/mural/gitlab2474/1646230185696?sender=u8acaa5f2dd7f92154f687467&key=c09ba7e6-10b0-4c15-bbf7-a28f1c89a36f)
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Read into https://gitlab.com/groups/gitlab-org/-/epics/7327 for performance testing
- [ ] Watch https://www.youtube.com/watch?v=EP3G-kjMDMs for performance testing
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/1

### March 14 - March 18

**Highest priorities**
- Responding to feedback on [Display more meaningful information for Artifacts page](https://gitlab.com/gitlab-org/gitlab/-/issues/33418)
- Technical discussions for [Improve communication of runners that are behind versions and need updates](https://gitlab.com/gitlab-org/gitlab/-/issues/339523)

✅ Completed ✅
- [x] Due March 14th: 2b in [Career development Mural](https://app.mural.co/t/gitlab2474/m/gitlab2474/1646230185696/299232df7d11c3208d324d634463261fee710ad2?sender=u8acaa5f2dd7f92154f687467)
- [x] Design for [Improve communication of runners that are behind versions and need updates](https://gitlab.com/gitlab-org/gitlab/-/issues/339523)
- [x] UX showcase prep
- [x] Responding to feedback on [Display more meaningful information for Artifacts page](https://gitlab.com/gitlab-org/gitlab/-/issues/33418)
- [x] Prep for solution validation for [Display more meaningful information for Artifacts page](https://gitlab.com/gitlab-org/gitlab/-/issues/33418)
- [x] Due 4/18: Ask RV about PM leaving and how it affects design buddy

↻ In progress ↻
- [ ] Interview training

❌ Not started ❌
- [ ] Due March 21: Fill out 3 and 4 with specific examples in [Mural](https://app.mural.co/invitation/mural/gitlab2474/1646230185696?sender=u8acaa5f2dd7f92154f687467&key=c09ba7e6-10b0-4c15-bbf7-a28f1c89a36f). Think of projects that are in front of you. For example, Pajamas - what would you have to do to contribute more or practice more within your stage group work?
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Read into https://gitlab.com/groups/gitlab-org/-/epics/7327 for performance testing
- [ ] Watch https://www.youtube.com/watch?v=EP3G-kjMDMs for performance testing
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/1

### March 7 - March 11

**Highest priorities**
- Responding to feedback on [Display more meaningful information for Artifacts page](https://gitlab.com/gitlab-org/gitlab/-/issues/33418)
- Design for [Improve communication of runners that are behind versions and need updates](https://gitlab.com/gitlab-org/gitlab/-/issues/339523)

✅ Completed ✅
- [x] Analyze customer meetings in dovetail
- [x] Summarize customer meeting from Jan 4th
- [x] Transfer items in [doc/dovetail](https://docs.google.com/document/d/1UfEWpa_eekPQ7aR9uYK8PICimD6uaNay4Vds8XbN_8M/edit) to issues (if needed)
- [x] Due March 7th: [Career development Mural](https://app.mural.co/t/gitlab2474/m/gitlab2474/1646230185696/299232df7d11c3208d324d634463261fee710ad2?sender=u8acaa5f2dd7f92154f687467)
- [x] Pajamas search empty state guidance https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1224

↻ In progress ↻
- [ ] Responding to feedback on [Display more meaningful information for Artifacts page](https://gitlab.com/gitlab-org/gitlab/-/issues/33418)
- [ ] Design for [Improve communication of runners that are behind versions and need updates](https://gitlab.com/gitlab-org/gitlab/-/issues/339523)
- [ ] Interview training

❌ Not started ❌
- [ ] Due March 14th: 2b in [Career development Mural](https://app.mural.co/t/gitlab2474/m/gitlab2474/1646230185696/299232df7d11c3208d324d634463261fee710ad2?sender=u8acaa5f2dd7f92154f687467)
- [ ] Due March 16th: UX showcase
- [ ] Create designs for [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Read into https://gitlab.com/groups/gitlab-org/-/epics/7327 for performance testing
- [ ] Watch https://www.youtube.com/watch?v=EP3G-kjMDMs for performance testing
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/1

### March 1 - March 4 (Short week with US holiday and F&F day)

**Highest priorities**
- Catching up on to-dos and emails from OOO time
- Summarizing and sharing runner enterprise customer meetings with the runner team

✅ Completed ✅
- [x] Issue to add more in-depth help to the user menu (followup with Justin)
- [x] Proposal for https://gitlab.com/gitlab-org/gitlab/-/issues/354006
- [x] Due March 1st: fill out https://forms.gle/ccYUtbE5cWN4fJeC6

↻ In progress ↻
- [ ] Analyze customer meetings in dovetail
- [ ] Summarize customer meeting from Jan 4th
- [ ] Transfer items in [doc/dovetail](https://docs.google.com/document/d/1UfEWpa_eekPQ7aR9uYK8PICimD6uaNay4Vds8XbN_8M/edit) to issues (if needed)

❌ Not started ❌
- [ ] Due March 7th: [Career development Mural](https://app.mural.co/t/gitlab2474/m/gitlab2474/1646230185696/299232df7d11c3208d324d634463261fee710ad2?sender=u8acaa5f2dd7f92154f687467)
- [ ] Due March 16th: UX showcase
- [ ] Pajamas search empty state guidance https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1224
- [ ] Create designs for [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Read into https://gitlab.com/groups/gitlab-org/-/epics/7327 for performance testing
- [ ] Watch https://www.youtube.com/watch?v=EP3G-kjMDMs for performance testing
- [ ] Check other handbook instances where Testing was being mentioned and fix to Pipeline Insights
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/1

----

### February ❄️⛄️🥶

----

### February 22 - February 24 (Short week with US holiday and F&F day)

**Highest priorities**
- Summarizing and sharing runner enterprise customer meetings
- Design for [Display more meaningful information for Artifacts page](https://gitlab.com/gitlab-org/gitlab/-/issues/33418)
- Design for [Improve communication of runners that are behind versions and need updates](https://gitlab.com/gitlab-org/gitlab/-/issues/339523)

✅ Completed ✅
- [x] UX scorecard issue template updates to use actionable insight templates
- [x] Discuss using actionable insight template for uxscorecard-rec issues with Rayana (followup with Adam)

↻ In progress ↻
- [ ] Analyze customer meetings in dovetail
- [ ] Summarize customer meeting from Jan 4th
- [ ] Transfer items in [doc/dovetail](https://docs.google.com/document/d/1UfEWpa_eekPQ7aR9uYK8PICimD6uaNay4Vds8XbN_8M/edit) to issues (if needed)

❌ Not started ❌
- [ ] Due March 1st: fill out https://forms.gle/ccYUtbE5cWN4fJeC6
- [ ] Due March 16th: UX showcase
- [ ] Pajamas search empty state guidance https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1224
- [ ] Create designs for [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] Issue to add more in-depth help to the user menu (followup with Justin)
- [ ] Work on [FY23 career development plan issue](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/65)
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Read into https://gitlab.com/groups/gitlab-org/-/epics/7327 for performance testing
- [ ] Watch https://www.youtube.com/watch?v=EP3G-kjMDMs for performance testing
- [ ] Check other handbook instances where Testing was being mentioned and fix to Pipeline Insights
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/1

### February 14 - February 18

**Highest priorities**
- Sharing out results from [UX scorecard for review apps](https://gitlab.com/gitlab-org/ux-research/-/issues/1630)
- Analyzing Runner customer meetings in dovetail
- Complete Runner UX page MR

✅ Completed ✅
- [x] Share UX scorecard for review apps
- [x] Finalize Runner UX page [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/96147)

↻ In progress ↻
- [ ] Analyze customer meetings in dovetail
- [ ] Summarize customer meeting from Jan 4th
- [ ] Transfer items in [doc/dovetail](https://docs.google.com/document/d/1UfEWpa_eekPQ7aR9uYK8PICimD6uaNay4Vds8XbN_8M/edit) to issues (if needed)

❌ Not started ❌
- [ ] Pajamas search empty state guidance https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1224
- [ ] Discuss using actionable insight template for uxscorecard-rec issues with Rayana (followup with Adam)
- [ ] Create designs for [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)
- [ ] Combine proposals in https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [ ] Issue to add more in-depth help to the user menu (followup with Justin)
- [ ] Work on [FY23 career development plan issue](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/65)
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Read into https://gitlab.com/groups/gitlab-org/-/epics/7327 for performance testing
- [ ] Watch https://www.youtube.com/watch?v=EP3G-kjMDMs for performance testing
- [ ] Check other handbook instances where Testing was being mentioned and fix to Pipeline Insights
- [ ] Merge artifacts research into research registry based on Erika's feedback https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/issues/1

### February 7 - February 11

**Highest priorities**
- [UX scorecard for review apps](https://gitlab.com/gitlab-org/ux-research/-/issues/1630)
- Analyzing Runner customer meetings in dovetail
- Complete Runner UX page MR

↻ In progress ↻
- [ ] Finalize Runner UX page [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/96147)
- [ ] Analyze customer meetings in dovetail
- [ ] Summarize customer meeting from Jan 4th
- [ ] Transfer items in [doc/dovetail](https://docs.google.com/document/d/1UfEWpa_eekPQ7aR9uYK8PICimD6uaNay4Vds8XbN_8M/edit) to issues (if needed)
- [ ] Share out performance testing JTBD research

❌ Not started ❌
- [ ] Work on [FY23 career development plan issue](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/65)
- [ ] Create designs for [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)
- [ ] Update severity labels on UX issues for testing
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Read into https://gitlab.com/groups/gitlab-org/-/epics/7327 for performance testing
- [ ] Watch https://www.youtube.com/watch?v=EP3G-kjMDMs for performance testing
- [ ] Check other handbook instances where Testing was being mentioned and fix to Pipeline Insights

### February 1 - February 4 (short week - OOO on January 31)

**Highest priorities**
- [UX scorecard for review apps](https://gitlab.com/gitlab-org/ux-research/-/issues/1630)

✅ Completed ✅
- [x] MR reviews for runner table
- [x] [Proposing workflows for testing collab](https://gitlab.com/gitlab-org/ux-research/-/issues/1719)
- [x] Ask for help with review app ux scorecard
- [x] Leave feedback for Veethika on https://gitlab.slack.com/archives/CLW71KM96/p1643891481392539 
- [x] Create some actionable insight issues from [artifacts research](https://gitlab.com/gitlab-org/ux-research/-/issues/1679)
- [x] Look into https://gitlab.com/gitlab-org/gitlab/-/issues/6061 (testing)
- [x] Finalize UX scorecard tasks and scenario
- [x] Mark which empty state to use for https://gitlab.com/gitlab-org/gitlab/-/issues/222941#note_819853541 
- [x] Proposal for https://gitlab.com/gitlab-org/gitlab/-/issues/352074
- [x] Proposal for [Runner maintenance field design](https://gitlab.com/gitlab-org/gitlab/-/issues/348299)
- [x] Update [testing workflow](https://gitlab.com/gitlab-org/ux-research/-/issues/1719) issue to account for [discussion](https://docs.google.com/document/d/1F-FwVysMIOUT7FNEdkj80USy1jQPfRvBoJ4G-GH2gco/edit) with ENF

↻ In progress ↻

❌ Not started ❌
- [ ] Finalize Runner UX page [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/96147)
- [ ] Update severity labels on UX issues for testing
- [ ] Analyze customer meetings in dovetail
- [ ] Summarize customer meeting from Jan 4th
- [ ] Transfer items in [doc/dovetail](https://docs.google.com/document/d/1UfEWpa_eekPQ7aR9uYK8PICimD6uaNay4Vds8XbN_8M/edit) to issues (if needed)
- [ ] Create issue for survey for customers to prioritize new features
- [ ] Create issue for sync (or async) session to prioritize features for enterprise management
- [ ] Share out performance testing JTBD research
- [ ] Update [testing workflow](https://gitlab.com/gitlab-org/ux-research/-/issues/1719) issue to account for [discussion](https://docs.google.com/document/d/1F-FwVysMIOUT7FNEdkj80USy1jQPfRvBoJ4G-GH2gco/edit) with ENF
- [ ] Jan 21: Leave feedback for https://gitlab.com/gitlab-org/gitlab/-/issues/346298/
- [ ] Add to UX debt and UI polish epics for runner https://gitlab.com/groups/gitlab-org/-/epics/5376
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Create designs for [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)
- [ ] Read into https://gitlab.com/groups/gitlab-org/-/epics/7327 for performance testing
- [ ] Watch https://www.youtube.com/watch?v=EP3G-kjMDMs for performance testing

----

### January 🏠❄️☃️

----

### January 24 - January 28

**Highest priorities**
- 👀 Reviewing MRs
- Responding to feedback on [design for bulk delete runners](https://gitlab.com/gitlab-org/gitlab/-/issues/339525)
- [Compile research for Artifacts:Testing](https://gitlab.com/gitlab-org/ux-research/-/issues/1679)
- [UX scorecard for review apps](https://gitlab.com/gitlab-org/ux-research/-/issues/1630)

✅ Completed ✅
- [x] Add images to https://gitlab.com/gitlab-org/gitlab/-/issues/351008/#user-interface-comparisons
- [x] Finalize [design for bulk delete runners](https://gitlab.com/gitlab-org/gitlab/-/issues/339525)
- [x] [Compile research for Testing](https://gitlab.com/gitlab-org/ux-research/-/issues/1679)

↻ In progress ↻
- [ ] Finalize [design for bulk delete runners](https://gitlab.com/gitlab-org/gitlab/-/issues/339525)
- [ ] [Compile research for Testing](https://gitlab.com/gitlab-org/ux-research/-/issues/1679)
- [ ] Look into https://gitlab.com/gitlab-org/gitlab/-/issues/6061 (testing)
- [ ] Look into https://gitlab.com/gitlab-org/gitlab/-/issues/297274 (testing)
- [ ] Finalize Runner UX page [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/96147)

❌ Not started ❌
- [ ] Update severity labels on UX issues for testing
- [ ] Analyze customer meetings in dovetail
- [ ] Summarize customer meeting from Jan 4th
- [ ] Transfer items in [doc/dovetail](https://docs.google.com/document/d/1UfEWpa_eekPQ7aR9uYK8PICimD6uaNay4Vds8XbN_8M/edit) to issues (if needed)
- [ ] Create issue for survey for customers to prioritize new features
- [ ] Create issue for sync (or async) session to prioritize features for enterprise management
- [ ] Share out performance testing JTBD research
- [ ] Update [testing workflow](https://gitlab.com/gitlab-org/ux-research/-/issues/1719) issue to account for [discussion](https://docs.google.com/document/d/1F-FwVysMIOUT7FNEdkj80USy1jQPfRvBoJ4G-GH2gco/edit) with ENF
- [ ] Jan 21: Leave feedback for https://gitlab.com/gitlab-org/gitlab/-/issues/346298/
- [ ] Add to UX debt and UI polish epics for runner https://gitlab.com/groups/gitlab-org/-/epics/5376
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Create designs for [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)
- [ ] Read into https://gitlab.com/groups/gitlab-org/-/epics/7327 for performance testing
- [ ] Watch https://www.youtube.com/watch?v=EP3G-kjMDMs for performance testing


### January 18 - January 21

**Highest priorities**
- 👀 Reviewing MRs
- Responding to feedback on [design for bulk delete runners](https://gitlab.com/gitlab-org/gitlab/-/issues/339525)
- [Compile research for Testing](https://gitlab.com/gitlab-org/ux-research/-/issues/1679)
- [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)

✅ Completed ✅
- [x] By Jan 20 : Watch training video for [interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/1712)
- [x] By Jan 20: Create discussion guide and share with Anne for [interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/1712)
- [x] By Jan 18: Look at issues listed in https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/60
- [x] Fill out expense report
- [x] Fill out [14.7 retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/47)
- [x] Fill out [14.7 testing retro](https://gitlab.com/gl-retrospectives/verify-stage/testing/-/issues/20)
- [x] Review MRs - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/78396#note_812787163, https://gitlab.com/gitlab-org/gitlab/-/merge_requests/78138

↻ In progress ↻
- [ ] Finalize [design for bulk delete runners](https://gitlab.com/gitlab-org/gitlab/-/issues/339525)
- [ ] [Compile research for Testing](https://gitlab.com/gitlab-org/ux-research/-/issues/1679)
- [ ] Look into https://gitlab.com/gitlab-org/gitlab/-/issues/6061 (testing)
- [ ] Look into https://gitlab.com/gitlab-org/gitlab/-/issues/297274 (testing)
- [ ] Create designs for [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)
- [ ] Update the [Runner UX strategy page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/verify/runner/) in the handbook and cross-link to [CICD UX page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/)

❌ Not started ❌
- [ ] Analyze customer meetings in dovetail
- [ ] Summarize customer meeting from Jan 4th
- [ ] Transfer items in [doc/dovetail](https://docs.google.com/document/d/1UfEWpa_eekPQ7aR9uYK8PICimD6uaNay4Vds8XbN_8M/edit) to issues (if needed)
- [ ] Create issue for survey for customers to prioritize new features
- [ ] Create issue for sync (or async) session to prioritize features for enterprise management
- [ ] Share out performance testing JTBD research
- [ ] Update [testing workflow](https://gitlab.com/gitlab-org/ux-research/-/issues/1719) issue to account for [discussion](https://docs.google.com/document/d/1F-FwVysMIOUT7FNEdkj80USy1jQPfRvBoJ4G-GH2gco/edit) with ENF
- [ ] Jan 21: Leave feedback for https://gitlab.com/gitlab-org/gitlab/-/issues/346298/
- [ ] Add to UX debt and UI polish epics for runner https://gitlab.com/groups/gitlab-org/-/epics/5376
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow


### January 10 - January 14

**Highest priorities**
- [Design for bulk delete runners](https://gitlab.com/gitlab-org/gitlab/-/issues/339525)
- [Compile research for Testing](https://gitlab.com/gitlab-org/ux-research/-/issues/1679)
- [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)

✅ Completed ✅
- [x] Fill out CI/CD agenda
- [x] Add async video update to CI/CD doc
- [x] Bring Jackie into conversation on https://gitlab.com/gitlab-org/ux-research/-/issues/1737
- [x] [Design for bulk delete runners](https://gitlab.com/gitlab-org/gitlab/-/issues/339525)
- [x] By Jan 17: https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/59
- [x] Try to get a design out for runner empty states

↻ In progress ↻
- [ ] Compile research for Testing](https://gitlab.com/gitlab-org/ux-research/-/issues/1679)
- [ ] Look into https://gitlab.com/gitlab-org/gitlab/-/issues/6061 (testing)
- [ ] Look into https://gitlab.com/gitlab-org/gitlab/-/issues/297274 (testing)
- [ ] Create designs for [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)
- [ ] Update the [Runner UX strategy page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/verify/runner/) in the handbook and cross-link to [CICD UX page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/)


❌ Not started ❌
- [ ] By Jan 20 : Watch training video for [interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/1712)
- [ ] By Jan 20: Create discussion guide and share with Anne for [interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/1712)
- [ ] Fill out expense report
- [ ] Analyze customer meetings in dovetail
- [ ] Transfer items in [doc/dovetail](https://docs.google.com/document/d/1UfEWpa_eekPQ7aR9uYK8PICimD6uaNay4Vds8XbN_8M/edit) to issues (if needed)
- [ ] Create issue for survey for customers to prioritize new features
- [ ] Create issue for sync (or async) session to prioritize features for enterprise management
- [ ] Summarize customer meeting from Jan 4th
- [ ] Add to UX debt and UI polish epics for runner https://gitlab.com/groups/gitlab-org/-/epics/5376
- [ ] Review/leave suggestions for https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75760
- [ ] Share out performance testing JTBD research
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Fill out [14.7 retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/47)

### January 4 - January 7 (Short week - F&F day Mon Jan 3)

**Highest priorities**
- [Design for bulk delete runners](https://gitlab.com/gitlab-org/gitlab/-/issues/339525)
- [Compile research for Testing](https://gitlab.com/gitlab-org/ux-research/-/issues/1679)
- [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)

✅ Completed ✅
- [x] Update the [Runner UX strategy page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/verify/runner/) in the handbook and cross-link to [CICD UX page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/)
- [x] Fill out https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/46 for 14.6 UX retro
- [x] Create new tasks file
- [x] Create issue and schedule for 14.8 to look into prepping for workspaces based on https://gitlab.com/gitlab-org/gitlab/-/issues/347099 -  include this issue [Audit and Document CI Permissions in the UI](https://gitlab.com/groups/gitlab-org/-/epics/6257#note_693892244) AND POST IN RUNNER GROUP
- [x] Read through emails
- [x] Clear todos
- [x] Customer meeting prep ([Agenda](https://docs.google.com/document/d/1bzqgy7NjEiBheXEHC35V_aq-RHM9mkT8J3scsRCp6Ng/edit))
- [x] Edit Performance Testing JTBD MR based on meeting with EF
- [x] Proposal for https://gitlab.com/gitlab-org/gitlab/-/issues/349794/
- [x] Proposal for https://gitlab.com/gitlab-org/gitlab/-/issues/349289
- [x] Add enterprise management customer meetings into dovetail: https://dovetailapp.com/projects/20llt4NmnpCGKyHfD1qmN7/readme
- [x] Create issue for slack discussion https://gitlab.slack.com/archives/CPANF553J/p1641593228010800 : https://gitlab.com/gitlab-org/gitlab/-/issues/349826/
- [x] Proposal for repository analytics issue : https://gitlab.com/gitlab-org/gitlab/-/issues/349826/

↻ In progress ↻
- [ ] [Design for bulk delete runners](https://gitlab.com/gitlab-org/gitlab/-/issues/339525)
- [ ] Compile research for Testing](https://gitlab.com/gitlab-org/ux-research/-/issues/1679)
- [ ] Look into https://gitlab.com/gitlab-org/gitlab/-/issues/6061 (testing)
- [ ] Look into https://gitlab.com/gitlab-org/gitlab/-/issues/297274 (testing)
- [ ] Create designs for [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)
- [ ] Update the [Runner UX strategy page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/verify/runner/) in the handbook and cross-link to [CICD UX page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/)

❌ Not started ❌
- [ ] Analyze customer meetings in dovetail
- [ ] Transfer items in [doc/dovetail](https://docs.google.com/document/d/1UfEWpa_eekPQ7aR9uYK8PICimD6uaNay4Vds8XbN_8M/edit) to issues (if needed)
- [ ] Create issue for survey for customers to prioritize new features
- [ ] Create issue for sync (or async) session to prioritize features for enterprise management
- [ ] Summarize customer meeting from Jan 4th
- [ ] Add to UX debt and UI polish epics for runner https://gitlab.com/groups/gitlab-org/-/epics/5376
- [ ] Review/leave suggestions for https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75760
- [ ] Try to get a design out for runner empty states
- [ ] Share out performance testing JTBD research
- [ ] By Jan 20 : Watch training video for [interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/1712)
- [ ] By Jan 20: Create discussion guide and share with Anne for [interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/1712)
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Fill out [14.7 retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/47)
