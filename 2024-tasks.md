## Weekly assigned tasks - 2024 📝

This file is meant to track and communicate my priorities and tasks on a weekly basis. I use my design tracking issues for issue and OKR statuses (ex: https://gitlab.com/gitlab-org/gitlab/-/issues/345031), which should be the SSOT of what design issues are being taken on for Runner Fleet for a specific milestone.

----

<details>
<summary markdown="span"> 🚀 Sometime in the near future 🚀 </summary>

- [ ] Create an MR to update "maturity" column on JTBD tables
- [ ] Read https://medium.com/@brandeismarshall/whats-unai-able-44b6cce1c0b7
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design#2238

</details>

----

### Dec 21 - Jan 6th

OOO

<details>
<summary markdown="span"> Previous months </summary>

## December ☃️🎄🎅

### Dec 16 - Dec 20

**Highest priorities**
- Summarizing insights from solution validation for immutable/protected tags
- Updating designs and breaking down into iterations

### Dec 9 - 13

**Highest priorities**
- Summarizing insights from solution validation for immutable/protected tags
- Updating designs

### Dec 2 - 5

**Highest priorities**
- Running solution validation for immutable/protected tags

## November 🍂🦃💨

### Nov 25 - 26

**Highest priorities**
- Running solution validation for immutable/protected tags

### Nov 18 - Nov 22

**Highest priorities**
- Immutable tags feature design
- Recruiting for solution validation

**Don't forget!**
- [x] Plan 17.8 design milestone issues
- [x] Open solution validation issue to recruit external users for immutable tags

### Nov 12 - Nov 15

**Highest priorities**
- Answer questions from Package settings redesign
- Immutable tags feature

**Don't forget!**
- [x] Open follow-up iterations for: (1) Updating kebab to delete for tags, (2) Showing examples of which tags the regex pattern impacts, (3) Showing a list of all tags the protection rule impacts
- [ ] Plan 17.7 and 17.8 design milestone issues
- [ ] Open solution validation issue to recruit external users for immutable tags

## October 🍂🍁💨

### Oct 28 - Nov 1

**Highest priorities**
- Protected tags project
- Onboarding to Package team

### Oct 15 - Oct 18

**Highest priorities**
- Tufts Capstone blog

### Oct 8 - Oct 11 (OOO Oct 7 🤒)

**Highest priorities**
- Tufts Capstone blog
- Running SUS outreach sessions

### Sep 30 - Oct 3rd (OOO 4th)

**Highest priorities**
- Tufts Capstone blog
- SUS outreach sessions setup

## September 🍃💨⛅️

### Sep 23 - 26 (OOO 24th & 27th)

**Highest priorities**
- OKR review
- Tufts Capstone recruitment
- SUS outreach sessions setup

### Sep 16 - Sep 20

**Highest priorities**
- Planning issue
- Summarize SUS data

### Sep 2 - Sep 6

**Highest priorities**
- Runner failure rate designs
- Prep for Roadmap Expectations call next week

## August 🍃🌦️☀️

### Aug 26th - Aug 30th

**Highest priorities:**
- Runner failure designs
- Reviewing MRs for Hackathon week
- Support SP during the transition to CI Steps

### Aug 19th - Aug 23rd

**Highest priorities:**
- Usability Improvement Week: https://gitlab.com/gitlab-org/gitlab/-/issues/474379
- Early Adopter Program survey creation

## July 🇺🇸👙😎

### July 29th - Aug 2nd

↻ In progress ↻
- [ ] Coverage issue updates 
- [ ] Early adopters surveys
- [ ] Early adopters list of customers https://docs.google.com/spreadsheets/d/1kRP5sOHB1TnrKAzMZYQUSpGoz6njw6MWNTnlfJjrQvY/edit?usp=sharing

### July 22nd - 26th

**Highest priorities:**
- Set up surveys for Early Adopters Program
- Runner failures design updates based on feedback

✅ Completed
- [x] Runners failures design

↻ In progress ↻
- [ ] Early adopters surveys
- [ ] Early adopters list of customers https://docs.google.com/spreadsheets/d/1kRP5sOHB1TnrKAzMZYQUSpGoz6njw6MWNTnlfJjrQvY/edit?usp=sharing

### July 15th - July 18th

**Highest priorities:**
- Set up surveys for Early Adopters Program
- Runner failures design
- Goal updates

↻ In progress ↻
- [ ] Runners failures design

❌ Not started ❌


### July 8th - July 12th

**Highest priorities:**
- Set up surveys for Early Adopters Program
- Runner failures design

↻ In progress ↻

❌ Not started ❌


### July 1st - 2nd - OOO for July 4th week vacation

## June ☀️🕶️🌻

### June 25th - 28th (June 24th vacation)

**Highest priorities:**
- Break down CI/CD analytics to many issues
- Add source filter to CI/CD analytics

✅ Completed ✅
- [x] Epic and issues for CI/CD analytics
- [x] Design source filter

↻ In progress ↻


❌ Not started ❌
- [ ] Watch https://youtu.be/yw3QLsC9EVk

### June 17th - 21st (Public holiday June 19th)

**Highest priorities:**
- Mid-year check-in prep
- Setting up environment for AI MR reviews

✅ Completed ✅
- [x] https://handbook.gitlab.com/handbook/people-group/talent-assessment/#mid-year-check-in

↻ In progress ↻

❌ Not started ❌
- [ ] Watch https://youtu.be/yw3QLsC9EVk

### June 10th - 13th (F&F day June 14th)

**Highest priorities:**
- CI steps validation research - summarizing and sharing insights

✅ Completed ✅
- [x] MR for https://gitlab.com/tufts-university/2024/tufts-university-capstone-2023-2024/-/issues/1
- [x] https://gitlab.slack.com/archives/CL9STLJ06/p1718025411419549

↻ In progress ↻
- [ ] https://handbook.gitlab.com/handbook/people-group/talent-assessment/#mid-year-check-in

❌ Not started ❌
- [ ] Watch https://youtu.be/yw3QLsC9EVk

### June 3rd - 7th (OOO June 5th)

**Highest priorities:**
- CI steps validation research sessions and recruiting

✅ Completed ✅
- [x] Send more invites out for steps research
- [x] SMART goals for career development

↻ In progress ↻
- [ ] MR for https://gitlab.com/tufts-university/2024/tufts-university-capstone-2023-2024/-/issues/1

❌ Not started ❌
- [ ] Watch https://youtu.be/yw3QLsC9EVk

## May 🌸🌼🌤️

### May 28th - 31st

**Highest priorities:**
- CI steps validation research sessions and recruiting

↻ In progress ↻
- [ ] SMART goals for career development
- [ ] Watch https://youtu.be/yw3QLsC9EVk

❌ Not started ❌
- [ ] Create recruiting request for project runners research

### May 22nd - May 24th

**Highest priorities:**
- Job logs design
- CI steps validation research recruiting

✅ Completed ✅
- [x] Capture fleet dashboard feedback from users in https://gitlab.com/gitlab-org/gitlab/-/issues/421737
- [x] Leave feedback on https://www.figma.com/proto/vY6JXwpV0fnFN3BdILLOrj/Hosted-Runners-for-Switchboard?page-id=25%3A2&node-id=25-109&viewport=771%2C465%2C0.25&t=n5CRhTuRPaZUbF2g-1&scaling=min-zoom&starting-point-node-id=36%3A369

↻ In progress ↻
- [ ] SMART goals for career development
- [ ] Watch https://youtu.be/yw3QLsC9EVk

❌ Not started ❌
- [ ] Create recruiting request for project runners research

### May 13th - May 17th

**Highest priorities:**
- Get up to speed with CI Steps
- 17.1 design issues
- Career development plan

↻ In progress ↻
- [ ] SMART goals for career development
- [ ] Leave feedback on https://www.figma.com/proto/vY6JXwpV0fnFN3BdILLOrj/Hosted-Runners-for-Switchboard?page-id=25%3A2&node-id=25-109&viewport=771%2C465%2C0.25&t=n5CRhTuRPaZUbF2g-1&scaling=min-zoom&starting-point-node-id=36%3A369

❌ Not started ❌
- [ ] Create recruiting request for project runners research

### May 6th - May 10th

**Highest priorities:**
- 17.1 design plan
- Career Development plan

✅ Completed ✅
- [x] Connect with GE about hosted runner needs 
- [x] Propose 17.1 plan for Runner
- [x] Clickhouse setup for runner

↻ In progress ↻
- [ ] SMART goals for career development

❌ Not started ❌
- [ ] Create recruiting request for project runners research

### April 29th - May 3rd (OOO May 2nd)

**Highest priorities:**
- 17.1 design plan
- Career Development plan

✅ Completed ✅
- [x] Questions on https://docs.google.com/document/d/1mBwhNebeLYQo0NhsdTYS-4bGaDxf9wHsCq8FYnpnGC8/edit
- [x] Career development plan FY25 step 3
- [x] Send email about EG project

↻ In progress ↻
- [ ] Propose 17.1 plan for Runner

❌ Not started ❌
- [ ] Connect with GE about hosted runner needs 
- [ ] SMART goals for career development
- [ ] Clickhouse setup for runner

## April 🌸🌷☔️

### April 22nd - 26th

**Highest priorities:**
- Job canvas MR
- Fleet Visibility UX Scorecard - Recommendations

✅ Completed ✅
- [x] Complete insights in dovetail
- [x] Complete UX scorecard recommendations

↻ In progress ↻
- [ ] Questions on https://docs.google.com/document/d/1mBwhNebeLYQo0NhsdTYS-4bGaDxf9wHsCq8FYnpnGC8/edit
- [ ] Career development plan FY25 step 3

❌ Not started ❌


### April 15th - 19th

**Highest priorities:**
- Job canvas feedback
- UX scorecard for Fleet Visibility

✅ Completed ✅
- [x] Respond to job canvas feedback
- [x] 1:1 doc RV
- [x] Finalize tasks for UX scorecard
- [x] Recruiting for usability test round 2 Tufts - https://calendly.com/guoc0818/30min - assigned to BLM

↻ In progress ↻


❌ Not started ❌
- [ ] Questions on https://docs.google.com/document/d/1mBwhNebeLYQo0NhsdTYS-4bGaDxf9wHsCq8FYnpnGC8/edit
- [ ] Career development plan FY25 step 3

### April 8th - April 12th 

OOO 🤒

## March 🌷🌱🍃

### March 25th - March 29th

**Highest priorities:**
- Improved Project CI/CD Analytics page into workfow::planning breakdown

✅ Completed ✅
- [x] https://gitlab.com/gitlab-org/competitor-evaluations/-/merge_requests/14#note_1738958288
- [x] https://docs.google.com/forms/d/e/1FAIpQLScnAVG42jTbL3kNYNfsS6jWN5xa5TvPAOGRjsI9baW5AMpj6g/viewform create quarterly feedback 
- [x] Career development plan for FY25 - steps 1 and 2

↻ In progress ↻

❌ Not started ❌
- [ ] Questions on https://docs.google.com/document/d/1mBwhNebeLYQo0NhsdTYS-4bGaDxf9wHsCq8FYnpnGC8/edit
- [ ] Career development plan FY25 step 3

### March 19th - March 22nd

**Highest priorities:**
- JTBD canvas: https://gitlab.com/gitlab-org/ux-research/-/issues/2910
- Plan for 16.11

✅ Completed ✅
- [x] AI research study

↻ In progress ↻

❌ Not started ❌
- [ ] https://gitlab.com/gitlab-org/competitor-evaluations/-/merge_requests/14#note_1738958288
- [ ] https://docs.google.com/forms/d/e/1FAIpQLScnAVG42jTbL3kNYNfsS6jWN5xa5TvPAOGRjsI9baW5AMpj6g/viewform create quarterly feedback 

### March 4th - March 8th

**Highest priorities:**
- JTBD canvas: https://gitlab.com/gitlab-org/ux-research/-/issues/2910

✅ Completed ✅
- [x] JTBD canvas for Review
- [x] Results for https://gitlab.com/gitlab-org/ux-research/-/issues/2123
- [x] Read https://about.gitlab.com/blog/2024/02/26/revisiting-the-variables-management-workflow
- [x] Create project runner issues

↻ In progress ↻
- [ ] Follow-up unmoderated for https://gitlab.com/gitlab-org/ux-research/-/issues/2123
- [ ] Customer feedback on https://gitlab.com/gitlab-org/gitlab/-/issues/323101/
- [ ] AI research study

❌ Not started ❌
- [ ] https://gitlab.com/gitlab-org/competitor-evaluations/-/merge_requests/14#note_1738958288
- [ ] https://docs.google.com/forms/d/e/1FAIpQLScnAVG42jTbL3kNYNfsS6jWN5xa5TvPAOGRjsI9baW5AMpj6g/viewform create quarterly feedback 

### February 26th - March 1st

**Highest priorities:**
- Analyze results from user testing: https://gitlab.com/gitlab-org/ux-research/-/issues/2123#top
- Job map: https://gitlab.com/gitlab-org/ux-research/-/issues/2910

✅ Completed ✅
- [x] Skill matrix
- [x] Safe training

↻ In progress ↻
- [ ] https://gitlab.com/gitlab-org/ux-research/-/issues/2123
- [ ] Customer feedback on https://gitlab.com/gitlab-org/gitlab/-/issues/323101/

❌ Not started ❌
- [ ] https://gitlab.com/gitlab-org/competitor-evaluations/-/merge_requests/14#note_1738958288
- [ ] https://docs.google.com/forms/d/e/1FAIpQLScnAVG42jTbL3kNYNfsS6jWN5xa5TvPAOGRjsI9baW5AMpj6g/viewform create quarterly feedback 
- [ ] Read https://about.gitlab.com/blog/2024/02/26/revisiting-the-variables-management-workflow/
- [ ] Create project runner issues

## February ☃️❄️🌨️

### February 20th - February 23rd

**Highest priorities:**
- https://gitlab.com/gitlab-org/ux-research/-/issues/2123

↻ In progress ↻
- [ ] https://gitlab.com/gitlab-org/ux-research/-/issues/2123

❌ Not started ❌
- [ ] https://gitlab.com/gitlab-org/competitor-evaluations/-/merge_requests/14#note_1738958288
- [ ] https://docs.google.com/forms/d/e/1FAIpQLScnAVG42jTbL3kNYNfsS6jWN5xa5TvPAOGRjsI9baW5AMpj6g/viewform create quarterly feedback form
- [ ] Review Runner Fleet backlog

### February 12th - February 16th

**Highest priorities:**
- https://gitlab.com/gitlab-org/ux-research/-/issues/2123

✅ Completed ✅
- [x] Create 16.10 and 16.11 issues - schedule OKRs

↻ In progress ↻
- [ ] https://gitlab.com/gitlab-org/ux-research/-/issues/2123

❌ Not started ❌
- [ ] https://gitlab.com/gitlab-org/competitor-evaluations/-/merge_requests/14#note_1738958288
- [ ] https://docs.google.com/forms/d/e/1FAIpQLScnAVG42jTbL3kNYNfsS6jWN5xa5TvPAOGRjsI9baW5AMpj6g/viewform create quarterly feedback form
- [ ] Review Runner Fleet backlog


### February 7th - February 9th

**Highest priorities:**
- Finishing 16.9 design issues

↻ In progress ↻
- [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/438418
- [ ] Review Runner Fleet backlog

❌ Not started ❌
- [ ] https://gitlab.com/gitlab-org/competitor-evaluations/-/merge_requests/14#note_1738958288

### January 29th - February 2nd

**Highest priorities:**
- Tufts capstone project prep
- Transferring findings from the runner creation flow Figjam to issues

✅ Completed ✅
- [x] Request NDA for Tufts capstone
- [x] Add benchmark study insights to resources markdown file
- [x] Add dovetail research to resources markdown file for Tufts: https://gitlab.dovetail.com/projects/2YgbGfU60gZybLse7gFSnF/v/4zqyYEG40YSiUuEjR4Gh7r
- [x] Transfer feedback to issues

↻ In progress ↻
- [ ] Review Runner Fleet backlog
- [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/440435

❌ Not started ❌
- [ ] https://gitlab.com/gitlab-org/competitor-evaluations/-/merge_requests/14#note_1738958288

## January ❄️☃️🥶

### January 22nd - 26th

✅ Completed ✅
- [x] Share competitor evaluation results
- [x] Keep eyes out on Tufts emails
- [x] Find times to meet
- [x] Reviewed feedback for new creation flow

↻ In progress ↻
- [ ] Transfer feedback to issues
- [ ] Review Runner Fleet backlog

❌ Not started ❌
- [ ] NDA for Tufts casptone

### January 16th - 19th

**Highest priorities**
- [Competitor evaluation for Fleet Visiblity](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/43)

✅ Completed ✅
- [x] Review competitors and summarize findings

↻ In progress ↻

❌ Not started ❌


### January 8th - 12th

**Highest priorities**
- [Competitor evaluation for Fleet Visiblity](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/43)

✅ Completed ✅
- [x] Share out insights from research audit for Fleet Visiblity https://gitlab.com/gitlab-org/ux-research/-/issues/2801#what-did-we-learn
- [x] Create project for Tufts casptone
- [x] Email Linda about updating Ultimate subscription
- [x] Fill out 16.9 planning https://docs.google.com/document/d/10r4OsOIfSx7UjidaZaPGmmusuRmtAU5qeh-yt_yBwS4/edit
- [x] Set up issue - https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/43

↻ In progress ↻


❌ Not started ❌
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text


</details>



