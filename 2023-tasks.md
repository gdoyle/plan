## Weekly assigned tasks - 2023 📝

This file is meant to track and communicate my priorities and tasks on a weekly basis. I use my design tracking issues for issue and OKR statuses (ex: https://gitlab.com/gitlab-org/gitlab/-/issues/345031), which should be the SSOT of what design issues are being taken on for Runner and Testing for a specific milestone.

----

<details>
<summary markdown="span"> 🚀 Sometime in the near future 🚀 </summary>

- [ ] Create an MR to update "maturity" column on JTBD tables
- [ ] Read https://medium.com/@brandeismarshall/whats-unai-able-44b6cce1c0b7

</details>

----

## December ❄️🎅⛄️

### Decembers 18th - 21st (OOO Dec 22nd)

**Highest priorities**
- Summarize insights from [audit CI insights existing research (ux-research#2801)](https://gitlab.com/gitlab-org/ux-research/-/issues/2801)

↻ In progress ↻
- [ ] Summarize CI insights research
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725 for CI insights
- [ ] Upload tag filter issue to https://gitlab.com/groups/gitlab-org/-/epics/11176
- [ ] Add investigation issue to for tag filter https://gitlab.com/groups/gitlab-org/-/epics/11176
- [ ] Update runner error rates design based on feedback

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### Decembers 12th - December 15th (OOO Dec 11th)

**Highest priorities**
- Summarize insights from [audit CI insights existing research (ux-research#2801)](https://gitlab.com/gitlab-org/ux-research/-/issues/2801)

↻ In progress ↻
- [ ] Summarize CI insights research
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725 for CI insights
- [ ] Upload tag filter issue to https://gitlab.com/groups/gitlab-org/-/epics/11176
- [ ] Add investigation issue to for tag filter https://gitlab.com/groups/gitlab-org/-/epics/11176
- [ ] Update runner error rates design based on feedback

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### Decembers 4th - December 8th (OOO Dec 5th & 6th)

**Highest priorities**
- [Audit CI insights existing research (ux-research#2801)](https://gitlab.com/gitlab-org/ux-research/-/issues/2801)
- Tufts capstone project prep

↻ In progress ↻
- [ ] Audit CI insights existing research
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725 for CI insights

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### November 29th - December 1st (OOO Nov 27th & 28th)

**Highest priorities**
- [Audit CI insights existing research (ux-research#2801)](https://gitlab.com/gitlab-org/ux-research/-/issues/2801)
- Tufts capstone project prep

✅ Completed ✅
- [x] Due Dec 1st: Set up voting for project proposals and tag the team
- [x] Due Dec 1st: Send an email to Linda & Nick to select projects

↻ In progress ↻
- [ ] Audit CI insights existing research
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725 for CI insights

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

## November 🦃🍂💨🦌

### November 20th - November 21st (OOO Nov 22nd - 28th)

↻ In progress ↻
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### November 14th - November 17th (OOO Nov 13th)

✅ Completed ✅
- [x] Break down https://gitlab.com/gitlab-org/gitlab/-/issues/423566#note_1641400305 into two epics
- [x] Fill details in https://gitlab.com/gitlab-org/ux-research/-/issues/2788
- [x] Respond to https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/131104
- [x] Iteration materials in career dev

↻ In progress ↻
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### November 6th - November 9th (Veteran's day Nov 10th)

✅ Completed ✅
- [x] Update on side projects
- [x] Add more to the TU handbook page re: questions in https://docs.google.com/document/d/1mBwhNebeLYQo0NhsdTYS-4bGaDxf9wHsCq8FYnpnGC8/edit?usp=sharing

↻ In progress ↻
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### October 30th - November 3rd

**Highest priorities**
- 16.6 design issues https://gitlab.com/gitlab-org/gitlab/-/issues/427937

✅ Completed ✅
- [x] Select benefits
- [x] Due Oct 29th: Update on blog post and runner dashboard next time. Tie the skills to these deliverables - communication, leadership, iteration, etc.
- [x] Create retro issue for Tufts project

↻ In progress ↻
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725
- [ ] Add more to the TU handbook page re: questions in https://docs.google.com/document/d/1mBwhNebeLYQo0NhsdTYS-4bGaDxf9wHsCq8FYnpnGC8/edit?usp=sharing

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

## October 🎃👻🍂🍁

### October 23rd - October 27th

**Highest priorities**
- 16.6 design issues https://gitlab.com/gitlab-org/gitlab/-/issues/427937

✅ Completed ✅
- [x] Complete TU issue https://gitlab.com/gitlab-org/gitlab-design/-/issues/2388
- [x] Open MR to add previous project ideas to https://about.gitlab.com/handbook/product/ux/learning-and-development/tufts-university-capstone-course/

↻ In progress ↻
- [ ] Due Oct 29th: Update on blog post and runner dashboard next time. Tie the skills to these deliverables - communication, leadership, iteration, etc.
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### October 16th - October 20th OOO

✅ Completed ✅
- [x] Talent assessment

### October 10th - October 13th

**Highest priorities**
- Summarizing insights from runner problem validation
- Adding designs for full report of runner usage

✅ Completed ✅
- [x] Draft blog post MR
- [x] Finish taking time off holidays
- [x] Create 16.6 design issuem and tag DE and RV
- [x] Create coverage issue
- [x] Summarize insights from runner PV

↻ In progress ↻
- [ ] Talent assessment

❌ Not started ❌
- [ ] Due Oct 29th: Update on blog post and runner dashboard next time. Tie the skills to these deliverables - communication, leadership, iteration, etc.
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725
- [ ] Create think big doc for cost AI problem. Add https://gitlab.slack.com/archives/C01RQ3AQH3R/p1691955235336479?thread_ts=1691674243.907289&cid=C01RQ3AQH3R to that doc.
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### October 2nd - October 5th

**Highest priorities**
- Summarizing insights from runner problem validation

✅ Completed ✅
- [x] full report of runner usage

↻ In progress ↻
- [ ] Summarize insights from runner PV

❌ Not started ❌
- [ ] Due Oct 29th: Update on blog post and runner dashboard next time. Tie the skills to these deliverables - communication, leadership, iteration, etc.
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725
- [ ] Create think big doc for cost AI problem. Add https://gitlab.slack.com/archives/C01RQ3AQH3R/p1691955235336479?thread_ts=1691674243.907289&cid=C01RQ3AQH3R to that doc.
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text


## September 🍁⛅️🍃

### September 25th - September 29th

**Highest priorities**
- Summarizing insights from runner problem validation
- 16.5 design issues
- Runner blog post draft

✅ Completed ✅
- [x] Review UXR training materials https://docs.google.com/document/d/1dtZRvYxEGNBvnK6cJ2eT1L5iYblnBGClHWvQyvvBeeQ/edit?usp=sharing
- [x] Blog post draft

↻ In progress ↻
- [ ] Summarize insights from runner PV
- [ ] full report of runner usage

❌ Not started ❌
- [ ] Due Oct 29th: Update on blog post and runner dashboard next time. Tie the skills to these deliverables - communication, leadership, iteration, etc.
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725
- [ ] Create think big doc for cost AI problem. Add https://gitlab.slack.com/archives/C01RQ3AQH3R/p1691955235336479?thread_ts=1691674243.907289&cid=C01RQ3AQH3R to that doc.
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### September 18th - September 21st (F&F day Sept 22nd)

✅ Completed ✅
- [x] Due Sept 15th: Sell RSUs
- [x] Due Sept 19th: Add UX showcase details & artifacts to issue
- [x] Due Sept 13th: Link https://gitlab.com/gitlab-org/gitlab/-/issues/419120 to a new issue for moving Runners nav item

↻ In progress ↻

❌ Not started ❌
- [ ] Due Oct 29th: Update on blog post and runner dashboard next time. Tie the skills to these deliverables - communication, leadership, iteration, etc.
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725
- [ ] Create think big doc for cost AI problem. Add https://gitlab.slack.com/archives/C01RQ3AQH3R/p1691955235336479?thread_ts=1691674243.907289&cid=C01RQ3AQH3R to that doc.
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### September 11th - September 15th

**Highest priorities**
- Recruit and summarize insights for runner cost problem validation
- 16.5 planning and organization

✅ Completed ✅
- [x] Create blog post issue for Fleet dashboard https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/8
- [x] Due Sept 11th: Update template to include learning from goals to match expectations
- [x] Due Sept 15th: Contribute template to handbook
- [x] Due Sept 11th: Create issues from customer feedback and update epics
- [x] Due Sept 15th: Update 401K plan

↻ In progress ↻
- [ ] Due Sept 13th: First iteration to update runner creation form
- [ ] Due Sept 13th: Link https://gitlab.com/gitlab-org/gitlab/-/issues/419120 to a new issue for moving Runners nav item
- [ ] Due Sept 15th: Sell RSUs
- [ ] Due Sept 19th: Add UX showcase details & artifacts to issue

❌ Not started ❌
- [ ] Due Sept 26th: Update on blog post and runner dashboard next time. Tie the skills to these deliverables - communication, leadership, iteration, etc.
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/26725
- [ ] Create think big doc for cost AI problem. Add https://gitlab.slack.com/archives/C01RQ3AQH3R/p1691955235336479?thread_ts=1691674243.907289&cid=C01RQ3AQH3R to that doc.
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### September 5th - September 8th

**Highest priorities**
- Recruit and summarize insights for runner cost problem validation
- Finalizing runner usage design

↻ In progress ↻
- [ ] Create think big doc for cost AI problem. Add https://gitlab.slack.com/archives/C01RQ3AQH3R/p1691955235336479?thread_ts=1691674243.907289&cid=C01RQ3AQH3R to that doc.
- [ ] Link https://gitlab.com/gitlab-org/gitlab/-/issues/419120 to a new issue for moving Runners nav item

❌ Not started ❌
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/2672
- [ ] Create blog post issue for Fleet dashboard https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85 
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

## August 🕶️🍃🌤️

### August 28th - August 31st

**Highest priorities**
- Recruit and summarize insights for runner cost problem validation
- Runner usage design

↻ In progress ↻
- [ ] Create think big doc for cost AI problem. Add https://gitlab.slack.com/archives/C01RQ3AQH3R/p1691955235336479?thread_ts=1691674243.907289&cid=C01RQ3AQH3R to that doc.
- [ ] Link https://gitlab.com/gitlab-org/gitlab/-/issues/419120 to a new issue for moving Runners nav item

❌ Not started ❌
- [ ] Fill out competitive analysis issue https://gitlab.com/gitlab-org/ux-research/-/issues/2672
- [ ] Create blog post issue for Fleet dashboard https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85 
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### August 14th - August 18th

----

**Highest priorities**
- Prep discussion guide for cost problem validation
- Runner usage design

✅ Completed ✅
- [x] Video summary of terminology effort
- [x] Share out terminology results and next steps
- [x] Create resource graph for runners
- [x] Book flights for product meetup

↻ In progress ↻
- [ ] Create think big doc for cost AI problem. Add https://gitlab.slack.com/archives/C01RQ3AQH3R/p1691955235336479?thread_ts=1691674243.907289&cid=C01RQ3AQH3R to that doc.
- [ ] Link https://gitlab.com/gitlab-org/gitlab/-/issues/419120 to a new issue for moving Runners nav item

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

----

### August 14th - August 18th

----

**Highest priorities**
- Hiring interviews
- Analysis for Runner terminology survey

✅ Completed ✅
- [x] Problem validation issue for runner costs
- [x] Scorecard for PD position
- [x] Due Aug 21st: https://gitlab.com/gitlab-org/gitlab-design/-/issues/2336
- [x] Start scheduling actionable insights from mental model research
- [x] Review terminology survey results
- [x] Create PV for AI and runner cost/resource
- [x] Create recruiting issue for PV for runner costs
- [x] Stepper MR feedback

↻ In progress ↻
- [ ] Create think big doc for cost AI problem. Add https://gitlab.slack.com/archives/C01RQ3AQH3R/p1691955235336479?thread_ts=1691674243.907289&cid=C01RQ3AQH3R to that doc.
- [ ] Create resource graph for runners
- [ ] Link https://gitlab.com/gitlab-org/gitlab/-/issues/419120 to a new issue for moving Runners nav item

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text


### August 7th - August 11th

----

**Highest priorities**
- Stepper MR
- Runner cost visiblity issue / Think Big analysis
- Monitor Runner terminology survey

✅ Completed ✅
- [x] Review qualtrics survey for terminology survey
- [x] stepper MR
- [x] 16.4 UX plan

↻ In progress ↻
- [ ] Link https://gitlab.com/gitlab-org/gitlab/-/issues/419120 to a new issue for moving Runners nav item
- [ ] Create think big doc for cost AI problem. Add https://gitlab.slack.com/archives/C01RQ3AQH3R/p1691955235336479?thread_ts=1691674243.907289&cid=C01RQ3AQH3R to that doc.
- [ ] Review terminology survey results
- [ ] Create resource graph for runners
- [ ] Start scheduling actionable insights from mental model research
- [ ] Problem validation issue for runner costs

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

----

### July 31st - August 4th

----

**Highest priorities**
- Catch up from OOO
- Monitor runner terminology survey

✅ Completed ✅
- [x] How to be a good mentor/mentee course
- [x] reach out to #people-connect about 360 feedback cycle
- [x] reach out to GW about product get together
- [x] send out mental model study summary
- [x] reach out to JP about mentoring

↻ In progress ↻
- [ ] stepper MR
- [ ] Link https://gitlab.com/gitlab-org/gitlab/-/issues/419120 to a new issue for moving Runners nav item

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

----

## July 🇺🇸☀️🏖️

----

### July 17th - July 20th

----

**Highest priorities**
- Mental model research write-up
- 16.3 design issues

✅ Completed ✅
- [x] 16.3 design planning issue
- [x] confirm state of stepper component
- [x] summarize mental model research https://docs.google.com/presentation/d/1py_IeZMNRg4Xro-vxVlWedJTZMttXQyY_RF92XtiVNE/edit?usp=sharing

↻ In progress ↻
- [ ] stepper MR

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

----

### July 10th - July 14th

----

**Highest priorities**
- Mental model research
- 16.2 design issues
- 16.3 planning

✅ Completed ✅
- [x] start summarizing data in Figma
- [x] add career dev feedback to threads in issue
- [x] make updates to career dev plan based on feedback

↻ In progress ↻
- [ ] 16.3 design planning issue
- [ ] confirm state of stepper component
- [ ] summarize mental model research https://docs.google.com/presentation/d/1py_IeZMNRg4Xro-vxVlWedJTZMttXQyY_RF92XtiVNE/edit?usp=sharing

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

✅ Completed ✅


### July 6th - July 7th (OOO June 30th - July 5th)

----

**Highest priorities**
- Mental model research
- Breaking down fleet dashboard MVC and answering questions

✅ Completed ✅
- [x] upload latest research sessions to dovetail
- [x] review migration button mr

↻ In progress ↻
- [ ] start summarizing data in Figma
- [ ] add career dev feedback to threads in issue
- [ ] make updates to plan based on feedback
- [ ] complete at least ONE issue in 16.2 plan

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text



### June 🎂☀️👙

----

### June 27th - June 29th (OOO June 26th & June 30th)

**Highest priorities**
- Mental model research

✅ Completed ✅
- [x] Create dovetail project for mental model study
- [x] Set up July 17th 1:1 to look at mid-year check-in with RV
- [x] Send out message to specific group channels to get internal GitLab members for research
- [x] Due July 21st: [Mid-year check-in](https://about.gitlab.com/handbook/people-group/talent-assessment/#mid-year-check-in)

↻ In progress ↻
- [ ] upload latest research sessions to dovetail
- [ ] start summarizing data

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### June 20th - June 22nd (OOO June 19th & F&F day June 23rd)

**Highest priorities**
- Mental model research prep
- SMART goals for career development
- Interview training

✅ Completed ✅
- [x] Due June 27th: SMART goals for career development https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/77
- [x] Complete Step 1 of AI fit workshop
- [x] Qualtrics survey
- [x] EF 360 review
- [x] Due July 31st: Interview training on social talent

↻ In progress ↻
- [ ] Due July 21st: [Mid-year check-in](https://about.gitlab.com/handbook/people-group/talent-assessment/#mid-year-check-in)

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text


### June 12th - June 16th

**Highest priorities**
- Stepper component MR
- 16.2 planning

✅ Completed ✅
- [x] Respond to feedback on stepper component MR
- [x] List of issues frontend only
- [x] Add "source" links to database MR as to what the customer need is
- [x] Create handbook page for Tufts capstone

↻ In progress ↻
- [ ] Due June 27th: SMART goals for career development https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/77

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### June 5th - June 9th

**Highest priorities**
- Stepper component MR
- Tufts capstone handbook page

✅ Completed ✅
- [x] Security training
- [x] Stepper component MR
- [x] Part 3 for career development

↻ In progress ↻
- [ ] Respond to feedback on stepper component MR
- [ ] Create handbook page for Tufts capstone

❌ Not started ❌
- [ ] List of issues frontend only
- [ ] Add "source" links to database MR as to what the customer need is
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### May 31st - June 2nd (OOO May 29th & 30th)

**Highest priorities**
- Stepper component MR
- Tufts University summary issue

✅ Completed ✅
- [x] Create Tufts issue with a summary of their work

↻ In progress ↻
- [ ] Create handbook page for Tufts capstone

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

----

### May 🌷🪻

----

### May 22nd - May 25th (F&F day May 26th)

**Highest priorities**
- Career development step 1
- Break down dashboard to issues

✅ Completed ✅
- [x] Career development step 1 https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/70
- [x] Break down dashboard MVC https://gitlab.com/gitlab-org/gitlab/-/issues/390921 to smaller issues
- [x] `WON'T DO` Complete FY24 career development mural exercise

↻ In progress ↻
- [ ] Create Tufts issue with a summary of their work
- [ ] Create handbook page for Tufts capstone

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### May 15th - May 19th

**Highest priorities**
- Make any design updates based on feedback for [Solution Validation: Runner Fleet Dashboard](https://gitlab.com/gitlab-org/ux-research/-/issues/2403)
- Career development step 1

✅ Completed ✅
- [x] Create actionable insights
- [x] Finalize next steps for dashboard design
- [x] Respond to dashboard design feedback

↻ In progress ↻
- [ ] Career development step 1

❌ Not started ❌
- [ ] Create handbook page for Tufts capstone
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Complete FY24 career development mural exercise
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin tex

### May 8th - May 12th

**Highest priorities**
- Analyze results of [Solution Validation: Runner Fleet Dashboard](https://gitlab.com/gitlab-org/ux-research/-/issues/2403)

✅ Completed ✅
- [x] Create video for project new runner creation flow
- [x] Create video for group new runner creation flow
- [x] Finish analyzing dashboard sessions
- [x] Summarize results and pass by RV
- [x] Create video for admin area new reg flow https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/124201

↻ In progress ↻
- [ ] Create actionable insights
- [ ] Finalize next steps for dashboard design

❌ Not started ❌
- [ ] Add We should try to partner closely on your mental model related projects to gain efficiencies. Let's start by doing a cross-walk with competitor terms and key concepts that we can also leverage with our marketing and technical writing teams to help us onboard new users more quickly. I'll follow-up in slack and in 1/1s next week to move us forward on this. to an upcoming milestone
- [ ] Create handbook page for Tufts capstone
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Complete FY24 career development mural exercise
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### May 1st - May 5th

**Highest priorities**
- Running sessions for [Solution Validation: Runner Fleet Dashboard](https://gitlab.com/gitlab-org/ux-research/-/issues/2403)
- Stepper component documentation

✅ Completed ✅
- [x] Break down and share runner groups effort with the team

↻ In progress ↻
- [ ] Finish analyzing dashboard sessions

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Complete FY24 career development mural exercise
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

----

### April 🌧️🌈

----

### April 24th - 28th

**Highest priorities**
- Running sessions for [Solution Validation: Runner Fleet Dashboard](https://gitlab.com/gitlab-org/ux-research/-/issues/2403)
- Stepper component

✅ Completed ✅
- [x] Final updates to https://gitlab.com/gitlab-org/gitlab/-/issues/388854/designs/details-popover.png#note_1359539055
- [x] New issue related to https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/610 to define usage guidelines
- [x] Upload sessions for runner dashboard to dovetail
- [x] Start analyzing sessions for runner dashboard

↻ In progress ↻

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Complete FY24 career development mural exercise
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text


### April 18th - April 21st (OOO April 17th)

**Highest priorities**
- Running sessions for [Solution Validation: Runner Fleet Dashboard](https://gitlab.com/gitlab-org/ux-research/-/issues/2403)
- Update 15.11 to 16.0 design issue

✅ Completed ✅
- [x] Due April 18th: https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/68
- [x] Design for https://gitlab.com/gitlab-org/gitlab/-/issues/396757
- [x] Look into stepper design https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/610

↻ In progress ↻

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Complete FY24 career development mural exercise
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### April 11th - April 14th (OOO April 10th)

**Highest priorities**
- Running sessions for [Solution Validation: Runner Fleet Dashboard](https://gitlab.com/gitlab-org/ux-research/-/issues/2403)
- Design for https://gitlab.com/gitlab-org/gitlab/-/issues/396757
- Design for https://gitlab.com/gitlab-org/gitlab/-/issues/395646 (relates to [stepper design](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/610))

✅ Completed ✅
- [x] Send out message to the UX team that May 9th UX weekly will be designated for Tufts students to present
- [x] Design for https://gitlab.com/gitlab-org/gitlab/-/issues/395646
- [x] Create issue for Runner mental model research and tag EF
- [x] Upload tufts meeting recordings

↻ In progress ↻
- [ ] Design for https://gitlab.com/gitlab-org/gitlab/-/issues/396757
- [ ] Look into stepper design https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/610

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Complete FY24 career development mural exercise
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Due April 18th: https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/68

### April 4th - April 6th (OOO April 3rd & April 7th)

**Highest priorities**
- Actionable insights for https://gitlab.com/gitlab-org/ux-research/-/issues/2391
- Updates to https://gitlab.com/gitlab-org/gitlab/-/issues/388854 based on research ^
- Updates to https://gitlab.com/gitlab-org/gitlab/-/issues/390921 based on feedback
- Recruiting for https://gitlab.com/gitlab-org/ux-research/-/issues/2403

✅ Completed ✅
- [x] Create actionable insights for https://gitlab.com/gitlab-org/ux-research/-/issues/2391
- [x] Readout for https://gitlab.com/gitlab-org/ux-research/-/issues/2391

↻ In progress ↻
- [ ] Look into stepper design https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/610
- [ ] Create issue for Runner mental model research and tag EF

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Complete FY24 career development mural exercise
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Due April 18th: https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/68

----

### March ❄🥶

----

### March 27th - March 29th (OOO March 30th - April 3rd)

**Highest priorities**
- Solution validation summary and readout https://gitlab.com/gitlab-org/ux-research/-/issues/2391
- Updates to https://gitlab.com/gitlab-org/gitlab/-/issues/388854 based on research ^
- Updates to https://gitlab.com/gitlab-org/gitlab/-/issues/390921 based on feedback
- Recruiting for https://gitlab.com/gitlab-org/ux-research/-/issues/2403

✅ Completed ✅
- [x] Tag CS members about solution validation for runner dashboard
- [x] Due March 27th: Share update on https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/72 about how limits are working
- [x] Insights for https://gitlab.com/gitlab-org/ux-research/-/issues/2391
- [x] Answer https://gitlab.com/gitlab-org/gitlab/-/issues/398139
- [x] Summarize insights for runner groups/managers in https://gitlab.com/gitlab-org/ux-research/-/issues/2391
- [x] Record video walkthrough of insights for https://gitlab.com/gitlab-org/ux-research/-/issues/2391

↻ In progress ↻
- [ ] Create actionable insights for https://gitlab.com/gitlab-org/ux-research/-/issues/2391
- [ ] Look into stepper design https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/610
- [ ] Readout for https://gitlab.com/gitlab-org/ux-research/-/issues/2391
- [ ] Create issue for Runner mental model research and tag EF

❌ Not started ❌
- [ ] Open MRs for CI/CD UX page https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
- [ ] Complete FY24 career development mural exercise
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Due April 18th: https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/68


### March 20th - March 24th

**Highest priorities**
- Solution validation analysis https://gitlab.com/gitlab-org/ux-research/-/issues/2391
- Updates to https://gitlab.com/gitlab-org/gitlab/-/issues/390921

✅ Completed ✅
- [x] Send out OOO message to teams for March 30th - April 3rd
- [x] Assign RV as backup for ^
- [x] Upload solution validation videos to dovetail
- [x] Create solution validation issue for https://gitlab.com/gitlab-org/gitlab/-/issues/390921 and tag DE to get EE sessions
- [x] Close out 15.10 issue and update 15.11 issue

↻ In progress ↻
- [ ] Look into stepper design https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/610

❌ Not started ❌
- [ ] Complete FY24 career development mural exercise
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Due March 27th: Share update on https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/72 about how limits are working

### March 13th - March 17th

**Highest priorities**
- Solution validation sessions for https://gitlab.com/gitlab-org/ux-research/-/issues/2391
- Updates to https://gitlab.com/gitlab-org/gitlab/-/issues/390921

✅ Completed ✅
- [x] Prep for https://gitlab.com/gitlab-org/ux-research/-/issues/2391
- [x] Consider updating runner registration form to use https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76128
- [x] Create an issue for progress tracker to add to runner creation form
- [x] Brief eval on TU project for professor

↻ In progress ↻

❌ Not started ❌
- [ ] Complete FY24 career development mural exercise
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text

### March 6th - March 10th

**Highest priorities**
- Share design for https://gitlab.com/gitlab-org/gitlab/-/issues/388854 with UXers
- Design for https://gitlab.com/gitlab-org/gitlab/-/issues/390921

✅ Completed ✅
- [x] Watch UX weekly meeting recording
- [x] Finalize the design for https://gitlab.com/gitlab-org/gitlab/-/issues/388854
- [x] Create quick presentation for UX showcase?

↻ In progress ↻
- [ ] Prep for https://gitlab.com/gitlab-org/ux-research/-/issues/2391

❌ Not started ❌
- [ ] Complete FY24 career development mural exercise
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Consider updating runner registration form to use https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76128

### February 27th - March 3rd (OOO Feb 23rd - Mar 1st)

**Highest priorities**
- Read slacks and emails from OOO time
- Design for https://gitlab.com/gitlab-org/gitlab/-/issues/390921

✅ Completed ✅
- [x] Watch CI/CD meeting recording Mar 1st
- [x] Look at https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/120511

↻ In progress ↻
- [ ] Finalize the design for https://gitlab.com/gitlab-org/gitlab/-/issues/388854
- [ ] Watch UX weekly meeting recording

❌ Not started ❌
- [ ] Complete FY24 career development mural exercise
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Consider updating runner registration form to use https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76128

----

### February ❄☃️

----

### February 21st - February 22nd (OOO Feb 23rd - Mar 1st)

**Highest priorities**
- Design for https://gitlab.com/gitlab-org/gitlab/-/issues/388854
- Design for https://gitlab.com/gitlab-org/gitlab/-/issues/387882

✅ Completed ✅
- [x] First iteration for https://gitlab.com/gitlab-org/gitlab/-/issues/388854
- [x] First iteration for https://gitlab.com/gitlab-org/gitlab/-/issues/387882

↻ In progress ↻

❌ Not started ❌
- [ ] Complete FY24 career development mural exercise
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Consider updating runner registration form to use https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76128

### February 13th - February 17th

**Highest priorities**
- Skipping failed tests problem validation https://gitlab.com/gitlab-org/ux-research/-/issues/2310
- Runner creation UI for projects https://gitlab.com/gitlab-org/gitlab/-/issues/383143

✅ Completed ✅
- [x] Due 2/13: Send out message to channels about PTO 
https://gitlab.com/gitlab-org/gitlab-design/-/issues/2204
- [x] Discussion guide

↻ In progress ↻
- [ ] Due 2/10: Complete FY24 career development mural exercise

❌ Not started ❌
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Consider updating runner registration form to use https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76128

### February 6th - February 10th

**Highest priorities**
- Skipping failed tests problem validation https://gitlab.com/gitlab-org/ux-research/-/issues/2310
- Runner creation UI for projects https://gitlab.com/gitlab-org/gitlab/-/issues/383143

✅ Completed ✅
- [x] Due 2/10: Create FY24 career development issue
- [x] Send out invite to new pair designers
- [x] Due 2/10: Add comments to https://gitlab.com/gitlab-org/gitlab-design/-/issues/2203
- [x] Due 2/7: Create issue for a warning modal before enabling shared runners
- [x] Create actionable insights for https://gitlab.com/gitlab-org/ux-research/-/issues/2263
- [x] Add comment to Pedro about runner data

↻ In progress ↻
- [ ] Due 2/10: Complete FY24 career development mural exercise
- [ ] Finish filling out https://gitlab.com/gitlab-org/gitlab/-/issues/389486

❌ Not started ❌
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Consider updating runner registration form to use https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76128

### January 30th - February 3rd (OOO 🤒 Jan 31st - Feb 2nd)

↻ In progress ↻
- [ ] Due 1/27: Create FY24 career development issue
- [ ] Finish filling out https://gitlab.com/gitlab-org/gitlab/-/issues/389486

❌ Not started ❌
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Consider updating runner registration form to use https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76128

----

### January ☃️❆

----

### January 23rd - January 27th

**Highest priorities**
- Tufts Capstone project prep
- FY24 career development issue

✅ Completed ✅
- [x] Due 1/23: Create MR to update runner workflow
- [x] Due 1/27: Complete all Tufts prep issues
- [x] Due 1/27: Record video update for Tufts project and share
- [x] Due 1/27: Send out meeting for student meet-up
- [x] Add comment to PI planning issue for MF

↻ In progress ↻
- [ ] Due 1/27: Create FY24 career development issue
- [ ] Finish filling out https://gitlab.com/gitlab-org/gitlab/-/issues/389486

❌ Not started ❌
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Consider updating runner registration form to use https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76128

### January 17th - January 20th (OOO Jan 16th for public holiday)

**Highest priorities**
- Potentially update solution validation to use UserTesting individuals
- Tufts Capstone project prep

✅ Completed ✅
- [x] Create prototype for flaky tests
- [x] Create test plan for flaky tests 
- [x] Due 1/17: https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/64
- [x] Due 1/18: Use https://docs.google.com/spreadsheets/d/1fa4pzDgbtXSbjw1hex-jouoYu_NDwHpwQwJMbcBHmI4/edit#gid=0 to update runner registration UI
- [x] Assigned projects issue

↻ In progress ↻
- [ ] Due 1/17: Create MR to update runner workflow
- [ ] Due 1/20: Create FY24 career development issue

❌ Not started ❌
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Consider updating runner registration form to use https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76128

### January 9th - January 12th (OOO Jan 13th for F&F day)

**Highest priorities**
- Solution validation for flaky tests
- Tufts Capstone project 

✅ Completed ✅
- [x] Flaky tests - add jobs info https://gitlab.com/gitlab-org/gitlab/-/issues/381799/designs/mr-widget.png

↻ In progress ↻
- [ ] Assigned projects issue
- [ ] Create prototype for flaky tests
- [ ] Create test plan for flaky tests 
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Create MR to update ways Runner team works with design issues
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Due Jan 17th: https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/64

❌ Not started ❌
- [ ] Create an issue to discuss customer office hours and tag RV

### January 3rd - January 6th (OOO Jan 2nd for NY day)

**Highest priorities**
- Solution validation for flaky tests

✅ Completed ✅
- [x] Send in expense report
- [x] Create meeting for runner registration process

↻ In progress ↻
- [ ] Assigned projects issue
- [ ] Flaky tests - add jobs info https://gitlab.com/gitlab-org/gitlab/-/issues/381799/designs/mr-widget.png
- [ ] Create prototype for flaky tests
- [ ] Create test plan for flaky tests 
- [ ] Add https://gitlab.slack.com/archives/C0SFP840G/p1671226354541889 to runner fleet customer feedback
- [ ] Create issue to document runner tag actions in UI
- [ ] Create issue to update group switch to enable stale runner cleanup to match admin text
- [ ] Create MR to update ways Runner team works with design issues
- [ ] Transfer https://gitlab.com/gitlab-org/gitlab/-/issues/366219 to issues
- [ ] Due Jan 17th: https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/64

❌ Not started ❌
- [ ] Create an issue to discuss customer office hours and tag RV

## 🥳 It's 2023!
