## Weekly assigned tasks - 2021 📝

This file is meant to track and communicate my priorities on a weekly basis. 

----

<details>
<summary markdown="span"> 🚀 Sometime in the near future 🚀 </summary>

- [ ] Update the [Runner UX strategy page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/verify/runner/) in the handbook and cross-link to [CICD UX page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/)
- [ ] Propose a solution for a timeline for UX onboarding issue
- [ ] Ask Taurie/Jeremy if foundations has documentation around deletion confirmation patterns
- [ ] Phase 2 of Runner Enterprise epic, Identify the job: What is it? How do we define it?
- [ ] Read - https://about.gitlab.com/handbook/engineering/ux/jobs-to-be-done/deep-dive/#what-is-a-jtbd , https://about.gitlab.com/handbook/engineering/ux/jobs-to-be-done/#jtbd-user-stories-and-tasks, https://about.gitlab.com/handbook/product/product-manager-role/learning-and-development/#-jobs-to-be-done (you can expense the books)
- [ ] Review [CI/CD comments](https://docs.google.com/document/d/1fCMY77lzlsjHaCeqEY6pIw_K4bS_JYlfYlLJpPPJfKQ/edit#) about PIC program and getting GitLab involved
- [ ] Review https://gitlab.com/groups/gitlab-org/-/epics/4257, https://gitlab.com/groups/gitlab-org/-/epics/2885, https://gitlab.com/groups/gitlab-org/-/epics/4419
- [ ] Add https://youtube.com/playlist?list=PL05JrBw4t0Kpj9T7ZNk2u4vGfRLSkgZis to Testing group page

</details>

----

### December ☃️ ❄️ 🎄

----

### December 20 - December 22 (Short week - 🎅☃️ OOO Dec 23 - Jan 3)

**Highest priorities**
- Sharing out performance testing JTBD research insights
- Finalizing performance testing JTBD MR
- MR reviews before OOO time

✅ Completed ✅
- [x] Fill out 14.6 UX retro
- [x] Fill out 14.6 Testing retro
- [x] Respond to https://gitlab.com/gitlab-org/ux-research/-/issues/1719 
- [x] Create 14.8 design issue
- [x] Edit Performance Testing JTBD MR based on meeting with EF
- [x] Create issue and schedule for 14.8 to look into prepping for workspaces based on https://gitlab.com/gitlab-org/gitlab/-/issues/347099 -  include this issue [Audit and Document CI Permissions in the UI](https://gitlab.com/groups/gitlab-org/-/epics/6257#note_693892244) AND POST IN RUNNER GROUP

↻ In progress ↻
- [ ] Review/leave suggestions for https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75760
- [ ] Add to UX debt and UI polish epics for runner https://gitlab.com/groups/gitlab-org/-/epics/5376
- [ ] Gina: Create issues that are needed from the [doc summary](https://docs.google.com/document/d/1UfEWpa_eekPQ7aR9uYK8PICimD6uaNay4Vds8XbN_8M/edit?usp=sharing)
- [ ] Create issue for survey to be sent out to customers around top priorities
- [ ] Create issue for internal team to get together and prioritize features

❌ Not started ❌
- [ ] Summarize customer meeting for runner
- [ ] Create designs for [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)
- [ ] Try to get a design out for runner empty states
- [ ] By Jan 20 : Watch training video for [interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/1712)
- [ ] By Jan 20: Create discussion guide and share with Anne for [interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/1712)
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Fill out https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/46 for 14.6 UX retro


### December 13 - December 17

**Highest priorities**
- Analyzing performance testing JTBD research

✅ Completed ✅
- [x] Send Vlad a list of testing questions for customers he meets with
- [x] Transfer [vision notes](https://docs.google.com/document/d/1CwAXFUcZEDILmvLROqh2ueFhqt5rru3HoNqDKIpjAW8/edit) to an issue (use https://gitlab.com/gitlab-org/gitlab/-/issues/343810 as a template)
- [x] Get an MR for JTBD revisions up for Performance Testing

↻ In progress ↻
- [ ] Review/leave suggestions for https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75760
- [ ] Respond to https://gitlab.com/gitlab-org/ux-research/-/issues/1719
- [ ] Add to UX debt and UI polish epics for runner https://gitlab.com/groups/gitlab-org/-/epics/5376
- [ ] Create issue and schedule for 14.8 to look into prepping for workspaces based on https://gitlab.com/gitlab-org/gitlab/-/issues/347099 -  include this issue [Audit and Document CI Permissions in the UI](https://gitlab.com/groups/gitlab-org/-/epics/6257#note_693892244) AND POST IN RUNNER GROUP

❌ Not started ❌
- [ ] Create designs for [Runner ux vision part 2](https://gitlab.com/gitlab-org/gitlab/-/issues/345594)
- [ ] Edit Performance Testing JTBD MR based on meeting with EF
- [ ] Try to get a design out for runner empty states
- [ ] By Jan 20 : Watch training video for [interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/1712)
- [ ] By Jan 20: Create discussion guide and share with Anne for [interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/1712)
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Fill out https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/46 for 14.6 UX retro

### November 29 - December 3

**Highest priorities**
- Coverage for DF and KM for Fri Dec 3 - reviewing any MRs
- Analyzing performance testing JTBD research
- Analzying runner solution validation research

✅ Completed ✅
- [x] Add https://gitlab.com/gitlab-org/gitlab/-/issues/343559 to 14.7
- [x] Set up interview with Participant 3 for performance testing
- [x] Set up interview with Participant 4 for performance testing
- [x] Complete discussion guide for solution validation https://docs.google.com/document/d/13yzbo9n3s46FkhQBpcyYZU1ZcwOerR_KR0Et9I_cExs/edit
- [x] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75655
- [x] By Jan 7th, add video to [FY'22 Pulse Engagement Survey](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12616)
- [x] By Dec 9, update UX coverage 2: https://gitlab.com/gitlab-org/gitlab-design/-/issues/1760
- [x] Upload latest videos from user interviews into Dovetail and starting analyzing
- [x] Send Vlad a list of testing questions for customers he meets with

↻ In progress ↻
- [ ] Review/leave suggestions for https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75760
- [ ] Add to UX debt and UI polish epics for runner https://gitlab.com/groups/gitlab-org/-/epics/5376
- [ ] Transfer [vision notes](https://docs.google.com/document/d/1CwAXFUcZEDILmvLROqh2ueFhqt5rru3HoNqDKIpjAW8/edit) to an issue (use https://gitlab.com/gitlab-org/gitlab/-/issues/343810 as a template)
- [ ] Create issue and schedule for 14.8 to look into prepping for workspaces based on https://gitlab.com/gitlab-org/gitlab/-/issues/347099 -  include this issue [Audit and Document CI Permissions in the UI](https://gitlab.com/groups/gitlab-org/-/epics/6257#note_693892244) AND POST IN RUNNER GROUP

❌ Not started ❌
- [ ] By Jan 20 : Watch training video for [interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/1712)
- [ ] By Jan 20: Create discussion guide and share with Anne for [interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/1712)
- [ ] Get an MR for JTBD revisions up for Performance Testing
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Fill out https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/46 for 14.6 UX retro

----

### November 🦃 🌬 🍂

----

### November 22 - November 23 (Short week - 🦃 OOO Nov 24 - Nov 26 🦃)

**Highest priorities**
- Finding 2 backend devs for Performance testing JTBD research https://docs.google.com/document/d/1zZYJ66CJP18Hibmm7o8KVek7hnmiB4dvD5aoCWcBc5k/edit#
- Finalizing Runner Configuration Details https://gitlab.com/gitlab-org/gitlab/-/issues/299758

✅ Completed ✅
- [x] Runner configuration iterations
- [x] Update [UX debt issues](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/51) tracker with testing issues
- [x] Update [UX scorecard rec issues](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/54) tracker with testing issues
- [x] By Nov 17, assign UX coverage 1: https://gitlab.com/gitlab-org/gitlab-design/-/issues/1759 to assignees and update the description

↻ In progress ↻
- [ ] Add to UX debt and UI polish epics for runner https://gitlab.com/groups/gitlab-org/-/epics/5376
- [ ] Add https://gitlab.com/gitlab-org/gitlab/-/issues/343559 to 14.7 milestone - pending response from Nadia
- [ ] Transfer [vision notes](https://docs.google.com/document/d/1CwAXFUcZEDILmvLROqh2ueFhqt5rru3HoNqDKIpjAW8/edit) to an issue (use https://gitlab.com/gitlab-org/gitlab/-/issues/343810 as a template)

❌ Not started ❌
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Create issue and schedule for an upcoming milestone to look at permissions in runner based on [Audit and Document CI Permissions in the UI](https://gitlab.com/groups/gitlab-org/-/epics/6257#note_693892244)
- [ ] By Dec 9, update UX coverage 2: https://gitlab.com/gitlab-org/gitlab-design/-/issues/1760
- [ ] By Jan 7th, add video to [FY'22 Pulse Engagement Survey](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12616)

### November 15 - November 19 (!Contribute Nov 16-18!)

**Highest priorities**
- Finding 2 backend devs for Performance testing JTBD research https://docs.google.com/document/d/1zZYJ66CJP18Hibmm7o8KVek7hnmiB4dvD5aoCWcBc5k/edit#
- Finalizing Runner Configuration Details https://gitlab.com/gitlab-org/gitlab/-/issues/299758
- 14.5 design issues https://gitlab.com/gitlab-org/gitlab/-/issues/339746

✅ Completed ✅
- [x] Come up with a list of ux scorecard rec and ux debt issues to be taken on in Q4
- [x] Update [UX debt issues](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/51) tracker with runner issues
- [x] Update [UX scorecard rec issues](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/54) tracker with runner issues

↻ In progress ↻
- [ ] Runner configuration iterations
- [ ] Update [UX debt issues](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/51) tracker with testing issues
- [ ] Update [UX scorecard rec issues](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/54) tracker with testing issues
- [ ] By Nov 17, assign UX coverage 1: https://gitlab.com/gitlab-org/gitlab-design/-/issues/1759 to assignees and update the description

❌ Not started ❌
- [ ] Add to UX debt and UI polish epics for runner https://gitlab.com/groups/gitlab-org/-/epics/5376
- [ ] Add https://gitlab.com/gitlab-org/gitlab/-/issues/343559 to 14.7 milestone - pending response from Nadia
- [ ] Transfer [vision notes](https://docs.google.com/document/d/1CwAXFUcZEDILmvLROqh2ueFhqt5rru3HoNqDKIpjAW8/edit) to an issue (use https://gitlab.com/gitlab-org/gitlab/-/issues/343810 as a template)
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] Create issue and schedule for an upcoming milestone to look at permissions in runner based on [Audit and Document CI Permissions in the UI](https://gitlab.com/groups/gitlab-org/-/epics/6257#note_693892244)
- [ ] By Dec 9, update UX coverage 2: https://gitlab.com/gitlab-org/gitlab-design/-/issues/1760
- [ ] By Jan 7th, add video to [FY'22 Pulse Engagement Survey](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12616)

### November 7 - November 12 (short week with public holiday on November 11)

**Highest priorities**
- Recruiting for Performance testing JTBD research https://docs.google.com/document/d/1zZYJ66CJP18Hibmm7o8KVek7hnmiB4dvD5aoCWcBc5k/edit#
- Finalizing Runner Configuration Details https://gitlab.com/gitlab-org/gitlab/-/issues/299758
- 14.5 design issues https://gitlab.com/gitlab-org/gitlab/-/issues/339746

✅ Completed ✅
- [x] By Nov 19 enroll in FSA https://gitlab.slack.com/archives/C0259241C/p1636389218449000 
- [x] Iterations to Runner configuration details based on feedback https://gitlab.com/gitlab-org/gitlab/-/issues/299758

↻ In progress ↻
- [ ] Add https://gitlab.com/gitlab-org/gitlab/-/issues/343559 to upcoming milestone - pending response from Nadia

❌ Not started ❌
- [ ] Transfer [vision notes](https://docs.google.com/document/d/1CwAXFUcZEDILmvLROqh2ueFhqt5rru3HoNqDKIpjAW8/edit) to an issue (use https://gitlab.com/gitlab-org/gitlab/-/issues/343810 as a template)
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] By Nov 18th, add a video for [FY'22 Pulse Engagement Survey](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/55)
- [ ] Add to UX debt and UI polish epics for runner https://gitlab.com/groups/gitlab-org/-/epics/5376
- [ ] Create issue and schedule for an upcoming milestone to look at permissions in runner based on [Audit and Document CI Permissions in the UI](https://gitlab.com/groups/gitlab-org/-/epics/6257#note_693892244)
- [ ] By Nov 17, assign UX coverage 1: https://gitlab.com/gitlab-org/gitlab-design/-/issues/1759 to assignees and update the description
- [ ] By Dec 9, update UX coverage 2: https://gitlab.com/gitlab-org/gitlab-design/-/issues/1760

### November 1 - November 5

**Highest priorities**
- Performance testing JTBD research https://docs.google.com/document/d/1zZYJ66CJP18Hibmm7o8KVek7hnmiB4dvD5aoCWcBc5k/edit#
- Finalizing Project quality summary designs https://gitlab.com/gitlab-org/gitlab/-/issues/337307

✅ Completed ✅
- [x] https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/53
- [x] Discussion guide for performance testing research https://docs.google.com/document/d/1zZYJ66CJP18Hibmm7o8KVek7hnmiB4dvD5aoCWcBc5k/edit#
- [x] Complete https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/53
- [x] 14.6 MILESTONE PLANNING ISSUE
- [x] Summary of convo with Nadia https://gitlab.com/gitlab-org/gitlab/-/issues/343559
- [x] Update milestone planning issues in Q4 with the [Q4 UX KRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1756)
- [x] Watch CI/CD meeting and look over agenda notes
- [x] Prep for all meetings tomorrow - make sure any action items were completed https://docs.google.com/document/d/1MVM1Ky79HQmNuFSx-uG5gutvRAUmnK0tI4S_7bE8ydI/edit#
- [x] By Nov 15th, UX coverage 1: https://gitlab.com/gitlab-org/gitlab-design/-/issues/1759
- [x] By Dec 9, create UX coverage 2: https://gitlab.com/gitlab-org/gitlab-design/-/issues/1760
- [x] Edits to [discussion guide for performance testing](https://docs.google.com/document/d/1zZYJ66CJP18Hibmm7o8KVek7hnmiB4dvD5aoCWcBc5k/edit#) based on feedback 
- [x] For Friday November 5th: Review and fill out https://gitlab.com/gitlab-org/gitlab-design/-/issues/1775 for 1:1 with Rayana on Nov 8th
- [x] Create 14.7 milestone planning!!!!!

↻ In progress ↻
- [ ] Iterations to Runner configuration details based on feedback https://gitlab.com/gitlab-org/gitlab/-/issues/299758
- [ ] Add https://gitlab.com/gitlab-org/gitlab/-/issues/343559 to upcoming milestone - pending response from Nadia

❌ Not started ❌
- [ ] Transfer [vision notes](https://docs.google.com/document/d/1CwAXFUcZEDILmvLROqh2ueFhqt5rru3HoNqDKIpjAW8/edit) to an issue (use https://gitlab.com/gitlab-org/gitlab/-/issues/343810 as a template)
- [ ] For an upcoming Friday: https://www.linkedin.com/learning/interaction-design-flow
- [ ] By Nov 18th, add a video for [FY'22 Pulse Engagement Survey](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/55)
- [ ] Add to UX debt and UI polish epics for runner https://gitlab.com/groups/gitlab-org/-/epics/5376
- [ ] Create issue and schedule for an upcoming milestone to look at permissions in runner based on [Audit and Document CI Permissions in the UI](https://gitlab.com/groups/gitlab-org/-/epics/6257#note_693892244)
- [ ] By Dec 9, update UX coverage 2: https://gitlab.com/gitlab-org/gitlab-design/-/issues/1760

----

### October 🎃 🍂 🍁

----

#### October 25 - October 29

**Highest priorities**
- Performance testing JTBD research https://docs.google.com/document/d/1zZYJ66CJP18Hibmm7o8KVek7hnmiB4dvD5aoCWcBc5k/edit#
- Finalizing Project quality summary designs https://gitlab.com/gitlab-org/gitlab/-/issues/337307

✅ Completed ✅
- [x] Iterations to Project Quality Summary based on feedback https://gitlab.com/gitlab-org/gitlab/-/issues/337307
- [x] Ask Rayana if performance testing should be considered [problem validation](https://gitlab.com/gitlab-org/ux-research/-/issues/1610#note_706831735) and if UX scorecard is a good approach for usability testing
- [x] Brainstorm Runner vision and UX process
- [x] By Oct 29 https://gitlab.com/gitlab-org/gitlab-design/-/issues/1753#results-team-matrix
- [x] Runner search and filter vision mocks in an issue
- [x] Runner search and filter vision recording
- [x] More project quality summary iterations
- [x] Put together proposal for the admin part of https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71607
- [x] Add a comment to the MR for Pedro and Miguel to help out with the extra backend code
- [x] Add UX scorecard issue for Usability Testing in epic
- [x] Respond to https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/52
- [x] Add severity to CI/CD UX debt and Scorecard-rec issues https://gitlab.com/gitlab-org/gitlab-design/-/issues/1742
- [x] Watch https://www.youtube.com/playlist?list=PL05JrBw4t0KoXcLvqKqlXlW9p_wmXIfez and start a convo with the testing team around setting up the code quality widget

↻ In progress ↻
- [ ] Discussion guide for performance testing research https://docs.google.com/document/d/1zZYJ66CJP18Hibmm7o8KVek7hnmiB4dvD5aoCWcBc5k/edit#
- [ ] Iterations to Runner configuration details based on feedback https://gitlab.com/gitlab-org/gitlab/-/issues/299758

❌ Not started ❌
- [ ] 14.6 MILESTONE PLANNING ISSUE. Also make one for 14.7 as a placeholder while you're at it!!!!!
- [ ] Summary of convo with Nadia https://gitlab.com/gitlab-org/gitlab/-/issues/343559
- [ ] Add to UX debt and UI polish epics for runner https://gitlab.com/groups/gitlab-org/-/epics/5376
- [ ] Testing JTBD page MR - add feature names?
- [ ] Create issue and schedule for upcoming milestone based on https://gitlab.com/groups/gitlab-org/-/epics/6257#note_693892244
- [ ] By Nov 15, UX coverage 1: https://gitlab.com/gitlab-org/gitlab-design/-/issues/1759
- [ ] By Dec 9, UX coverage 2: https://gitlab.com/gitlab-org/gitlab-design/-/issues/1760

#### October 18 - October 22 (Short week - OOO on October 19)

**Highest priorities**
- Edits to https://gitlab.com/gitlab-org/gitlab/-/issues/337307
- Finalizing https://gitlab.com/gitlab-org/gitlab/-/issues/340324
- Proposal for https://gitlab.com/gitlab-org/gitlab/-/issues/299758

✅ Completed ✅
- [x] Edits to Project Quality Summary https://gitlab.com/gitlab-org/gitlab/-/issues/337307
- [x] Solidfy design post-feedback for Runner table design is out-dated and doesn't allow for the addition of data: https://gitlab.com/gitlab-org/gitlab/-/issues/340324
- [x] UX skill mapping worksheet
- [x] Create Usability Testing follow-up epic and link to https://gitlab.com/gitlab-org/ux-research/-/issues/1557#note_710595426, close out research issue
- [x] Add recommendation issues of how to solve for research found in Usability Testing to epic
- [x] Add dogfooding issue to epic
- [x] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72529
- [x] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72705
- [x] Proposal for Improve usability of runner configuration details: https://gitlab.com/gitlab-org/gitlab/-/issues/299758
- [x] Figure out MVC approaches for the 14.4 Runner design issues and how we can make changes without having to validate - then prioritize ones that need validation

↻ In progress ↻
- [ ] Iterations to Project Quality Summary based on feedback https://gitlab.com/gitlab-org/gitlab/-/issues/337307
- [ ] Discussion guide for performance testing research https://docs.google.com/document/d/1zZYJ66CJP18Hibmm7o8KVek7hnmiB4dvD5aoCWcBc5k/edit#
- [ ] Brainstorm Runner vision and UX process

❌ Not started ❌
- [ ] Iterations to Runner configuration details based on feedback https://gitlab.com/gitlab-org/gitlab/-/issues/299758
- [ ] Runner search and filter vision mocks in an issue
- [ ] Runner search and filter vision recording
- [ ] Put together proposal for the admin part of https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71607
- [ ] Add a comment to the MR for Pedro and Miguel to help out with the extra backend code
- [ ] Add UX scorecard issue for Usability Testing in epic
- [ ] ???Ask Rayana if performance testing should be considered performance testing and if UX scorecard is a good approach for usability testing
- [ ] Testing JTBD page MR - add feature names?
- [ ] Create issue and schedule for upcoming milestone based on https://gitlab.com/groups/gitlab-org/-/epics/6257#note_693892244
- [ ] https://gitlab.com/gitlab-org/gitlab-design/-/issues/1742

#### October 12 - October 14 (SUPER short week with public holiday Mon and F&F day Fri)

**Highest priorities**
-  Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71607
- Solidfy design post-feedback for Runner table design is out-dated and doesn't allow for the addition of data: https://gitlab.com/gitlab-org/gitlab/-/issues/340324
- Proposal for Improve usability of runner configuration details: https://gitlab.com/gitlab-org/gitlab/-/issues/299758

✅ Completed ✅
- [x] GitLab team member survey
- [x] Fill out CI/CD agenda
- [x] Run Solution Validation https://gitlab.com/gitlab-org/ux-research/-/issues/1594
- [x] Analyze solution validation feedback and move design forward

↻ In progress ↻
- [ ] Proposal for Improve usability of runner configuration details: https://gitlab.com/gitlab-org/gitlab/-/issues/299758
- [ ] Solidfy design post-feedback for Runner table design is out-dated and doesn't allow for the addition of data: https://gitlab.com/gitlab-org/gitlab/-/issues/340324
- [ ] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71607

❌ Not started ❌
- [ ] Testing JTBD page MR - add feature names?
- [ ] Figure out MVC approaches for the 14.4 Runner design issues and how we can make changes without having to validate - then prioritize ones that need validation
- [ ] Create issue and schedule for upcoming milestone based on https://gitlab.com/groups/gitlab-org/-/epics/6257#note_693892244
- [ ] Brainstorm Runner vision and UX process
- [ ] https://gitlab.com/gitlab-org/gitlab-design/-/issues/1742

#### October 4 - October 8

**Highest priorities**
- Reviewing Browser Performance MR Widget Refactoring: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71330
- Proposal for Runner table design is out-dated and doesn't allow for the addition of data: https://gitlab.com/gitlab-org/gitlab/-/issues/340324
- Proposal for Improve usability of runner configuration details: https://gitlab.com/gitlab-org/gitlab/-/issues/299758
- Prepare solution validation for https://gitlab.com/gitlab-org/gitlab/-/issues/299509

✅ Completed ✅
- [x] Join a MR widgets meeting - scheduled for Thursday
- [x] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70993
- [x] Rework Usability Testing JTBDs based on feedback: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/91050
- [x] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71611
- [x] Cigna emails
- [x] Solidify Usability Testing JTBDs: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/91050
- [x] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71330
- [x] Proposal for Runner table design is out-dated and doesn't allow for the addition of data: https://gitlab.com/gitlab-org/gitlab/-/issues/340324
- [x] Prep for Solution Validation https://gitlab.com/gitlab-org/ux-research/-/issues/1594

↻ In progress ↻
- [ ] Proposal for Improve usability of runner configuration details: https://gitlab.com/gitlab-org/gitlab/-/issues/299758
- [ ] Solidfy design post-feedback for Runner table design is out-dated and doesn't allow for the addition of data: https://gitlab.com/gitlab-org/gitlab/-/issues/340324
- [ ] Run Solution Validation https://gitlab.com/gitlab-org/ux-research/-/issues/1594

❌ Not started ❌
- [ ] Testing backlog issues with UX tag
- [ ] Testing JTBD page MR - add feature names?
- [ ] Figure out MVC approaches for the 14.4 Runner design issues and how we can make changes without having to validate - then prioritize ones that need validation
- [ ] Brainstorm Runner vision and UX process
- [ ] Review [CI/CD comments](https://docs.google.com/document/d/1fCMY77lzlsjHaCeqEY6pIw_K4bS_JYlfYlLJpPPJfKQ/edit#) about PIC program and getting GitLab involved
- [ ] Create issue and schedule for upcoming milestone based on https://gitlab.com/groups/gitlab-org/-/epics/6257#note_693892244

----

### September 🍁 🥀

----

#### September 27 - September 30 (short week with PTO on 1st)

**Highest priorities**
- Solidifying Usability Testing JTBDs: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/91050
- Solidfying Proposal for Improve location of where runner registration info lives: https://gitlab.com/gitlab-org/gitlab/-/issues/299509
- Proposal for Runner table design is out-dated and doesn't allow for the addition of data: https://gitlab.com/gitlab-org/gitlab/-/issues/340324

✅ Completed ✅
- [x] Create solution validation placeholder for Improve location: https://gitlab.com/gitlab-org/ux-research/-/issues/1594
- [x] Ask for design feedback on the proposals of runner registration info
- [x] First draft of Usability Testing JTBDs: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/91050
- [x] Ask for Suzanne's opinion on wording for Improve location of where runner registration info lives: https://gitlab.com/gitlab-org/gitlab/-/issues/299509
- [x] Read https://about.gitlab.com/handbook/engineering/ux/product-designer/#working-on-issues and watch the recording 

↻ In progress ↻
- [ ] Proposal for Runner table design is out-dated and doesn't allow for the addition of data: https://gitlab.com/gitlab-org/gitlab/-/issues/340324
- [ ] Rework Usability Testing JTBDs based on feedback: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/91050

❌ Not started ❌
- [ ] Testing backlog issues with UX tag
- [ ] Testing JTBD page MR - add feature names?
- [ ] Cigna emails
- [ ] Figure out MVC approaches for the 14.4 Runner design issues and how we can make changes without having to validate - then prioritize ones that need validation
- [ ] Join a MR widgets meeting
- [ ] Brainstorm Runner vision and UX process
- [ ] Review [CI/CD comments](https://docs.google.com/document/d/1fCMY77lzlsjHaCeqEY6pIw_K4bS_JYlfYlLJpPPJfKQ/edit#) about PIC program and getting GitLab involved

#### September 20 - September 23 (short week with f&f day on 24th)

**Highest priorities**
- Usability Testing JTBD validation interviews: https://gitlab.com/gitlab-org/ux-research/-/issues/1557 
- Improve location of where runner registration info lives: https://gitlab.com/gitlab-org/gitlab/-/issues/299509

✅ Completed ✅
- [x] Watch CI/CD meeting
- [x] Insider trader training
- [x] Check out Marcel's GitDock comment https://gitlab.slack.com/archives/CL9STLJ06/p1632307431124200

↻ In progress ↻
- [ ] Improve location of where runner registration info lives: https://gitlab.com/gitlab-org/gitlab/-/issues/299509
- [ ] Runner table design is out-dated and doesn't allow for the addition of data: https://gitlab.com/gitlab-org/gitlab/-/issues/340324

❌ Not started ❌
- [ ] Create solution validation placeholder for Improve location
- [ ] Ask for feedback on the proposals of runner registration info
- [ ] Cigna emails
- [ ] Review [CI/CD comments](https://docs.google.com/document/d/1fCMY77lzlsjHaCeqEY6pIw_K4bS_JYlfYlLJpPPJfKQ/edit#) about PIC program and getting GitLab involved

#### September 13 - September 17

**Highest priorities**
- Usability Testing JTBD validation interviews https://gitlab.com/gitlab-org/ux-research/-/issues/1557 
- 14.4 design issues 

**Task list**

✅ Completed ✅
- [x] Catch up with Scott about MR widgets in 14.4 and reviewing
- [x] Post in #frontend and #whats-happening-at-gitlab for recruiting
- [x] Complete getting 14.4 Runner issues ready
- [x] Start [conversation](https://gitlab.com/gitlab-org/gitlab/-/issues/340838) with Miguel around register runners button
- [x] Sign Bamboo HR policies
- [x] Leave something on [14.3 UX retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/42)
- [x] UX transition issue [#1657](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1657)
- [x] Schedule Usability Testing JTBD validation interviews https://gitlab.com/gitlab-org/ux-research/-/issues/1557 

❌ Not started ❌
- [ ] Review [CI/CD comments](https://docs.google.com/document/d/1fCMY77lzlsjHaCeqEY6pIw_K4bS_JYlfYlLJpPPJfKQ/edit#) about PIC program and getting GitLab involved

#### September 7 - September 10

**Highest priorities**
- Usability Testing JTBD validation interviews https://gitlab.com/gitlab-org/ux-research/-/issues/1557 
- Breaking down Runner 14.4 design issues

**Task list**

✅ Completed ✅
- [x] Review Nadia's links (https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/2021-week-33.md, https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/46)
- [x] Send out message to #ux, #ux_coworking, #product channels to gather participants for user interviews
- [x] Upload 1:1 with Rayana to Unfiltered - private


↻ In progress ↻
- [ ] Schedule Usability Testing JTBD validation interviews https://gitlab.com/gitlab-org/ux-research/-/issues/1557 
- [ ] UX transition issue [#1657](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1657)
- [ ] Complete getting 14.4 Runner issues ready
- [ ] Leave something on [14.3 UX retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/42)

❌ Not started ❌
- [ ] Review [CI/CD comments](https://docs.google.com/document/d/1fCMY77lzlsjHaCeqEY6pIw_K4bS_JYlfYlLJpPPJfKQ/edit#) about PIC program and getting GitLab involved

#### August 30 - September 3

✅ Completed ✅
- [x] Add weights to 14.3 milestone items
- [x] Runner Enterprise Scorecard UX KR - Create recommendations [#1666](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1666)
- [x] Create 14.4 and 14.5 planning issues (remember to add weights)
- [x] Update https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/68#user-experience-roller_coaster
- [x] Open issue to discuss priorities for https://gitlab.com/groups/gitlab-org/-/epics/4015
- [x] Create threads on 14.4 milestone planning issue with James and Darren
- [x] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68966#note_661061209 and follow up with Darren 1:1

↻ In progress ↻
- [ ] UX transition issue [#1657](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1657)
- [ ] Complete getting 14.4 Runner issues ready

❌ Not started ❌
- [ ] Leave something on [14.3 UX retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/42)
- [ ] Review Nadia's links (https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/2021-week-33.md, https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/46)
- [ ] Review [CI/CD comments](https://docs.google.com/document/d/1fCMY77lzlsjHaCeqEY6pIw_K4bS_JYlfYlLJpPPJfKQ/edit#) about PIC program and getting GitLab involved

----

### August 🌸 🍃

----

#### August 23 - August 26
**August 27 PTO for Family and Friends Day**

✅ Completed ✅
- [x] Add something to [CI/CD UX meeting agenda](https://docs.google.com/document/d/1fCMY77lzlsjHaCeqEY6pIw_K4bS_JYlfYlLJpPPJfKQ/edit#) for feedback 
- [x] 14.4 milestone planning issue
- [x] Runner Enterprise Scorecard UX KR - Heuristics evaluation [#1666](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1666)
- [x] Finish watching https://www.youtube.com/watch?v=aZcl6bomBkU
- [x] Runner Enterprise Scorecard UX KR - Create walkthrough [#1666](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1666)
- [x] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68966
- [x] James/Gina 1:1 : Add note to Review JTBD for performance and usability testing related to UX KR work

↻ In progress ↻
- [ ] Runner Enterprise Scorecard UX KR - Create recommendations [#1666](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1666)
- [ ] UX transition issue [#1657](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1657)
- [ ] Add weights to 14.3 milestone items

❌ Not started ❌
- [ ] Review Nadia's links (https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/2021-week-33.md, https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/46)
- [ ] Review [CI/CD comments](https://docs.google.com/document/d/1fCMY77lzlsjHaCeqEY6pIw_K4bS_JYlfYlLJpPPJfKQ/edit#) about PIC program and getting GitLab involved
- [ ] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68966#note_661061209 and follow up with Darren 1:1

#### August 16 - August 20

✅ Completed ✅
- [x] Ask Scott to add you to Testing group to view/contribute to milestone retros
- [x] Speaker notes for UX showcase
- [x] Add something to [14.2 Testing retro](https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/69)
- [x] Read and become familiar with the [Code Review Guidelines ](https://docs.gitlab.com/ee/development/code_review.html#approval-guidelines)
- [x] Read conversation in https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/87145 
- [x] Add something to [14.2 UX retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/39)
- [x] Runner Enterprise scorecard prep (UX OKR) [#1666](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1666)
- [x] Boston high school UX workshop [blog draft](https://docs.google.com/document/d/1DZZs8hiJmhvYcCNy8QlWte27OWFKF3OKLSgtmuo5kOU/edit?usp=sharing)

↻ In progress ↻
- [ ] UX transition issue [#1657](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1657)
- [ ] Review JTBD for performance and usability testing related to UX KR work

❌ Not started ❌
- [ ] CI/CD UX meeting cribs video
- [ ] Review Nadia's links (https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/2021-week-33.md, https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/46)

#### August 9 - August 13

✅ Completed ✅
- [x] Prepare UX onboarding & UX buddy presentation for the CI/CD UX showcase on August 18th (more details in [#47](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/47)) , [Figma](https://www.figma.com/file/M1KCVvigl4cfWniOLDb1JP/Gina-UX-Showcase-Aug-18?node-id=0%3A1)
- [x] Review Testing product feature videos [#337854](https://gitlab.com/gitlab-org/gitlab/-/issues/337854)
- [x] UX onboarding issue [#1648](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1648)
- [x] Review runner docs [here](https://docs.google.com/document/d/1vKqCkk5jG7-cNfzhiGGArkMcd76BoeUHj1fRglcw-oI/edit)

↻ In progress ↻
- [ ] Runner Enterprise scorecard prep (UX OKR) [#1666](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1666)
- [ ] UX transition issue [#1657](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1657)

#### August 2 - August 6

✅ Completed ✅
- [x] Review testing docs [here](https://docs.google.com/document/d/17AgZtfxyOE6kE62gvFbZTx9A69OqNSobZ77U-T_YGVY/edit?usp=sharing)
- [x] Create issue for reviewing Testing product feature videos [#337854](https://gitlab.com/gitlab-org/gitlab/-/issues/337854)

↻ In progress ↻
- [ ] Runner Enterprise scorecard prep (UX OKR) [#1666](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1666)
- [ ] Prepare UX onboarding & UX buddy presentation for the CI/CD UX showcase on August 18th (more details in [#47](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/47))
- [ ] UX onboarding issue [#1648](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1648)
- [ ] UX transition issue [#1657](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1657)
- [ ] Review runner docs [here](https://docs.google.com/document/d/1vKqCkk5jG7-cNfzhiGGArkMcd76BoeUHj1fRglcw-oI/edit)

----

### July ☀️🌻

----

#### July 26 - July 30

✅ Completed ✅
- [x] Create access request issue for Loom and tag V & Rayana
- [x] Record video of experience on Loom and include in CI/CD meeting agenda
- [x] Create access request issue for Respondent.io (not Ally anymore since signing in worked)
- [x] Weeks 2-4 onboarding product designer issue [#2923](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2923)
- [x] Send out [design pair](https://about.gitlab.com/handbook/engineering/ux/how-we-work/#pair-designing) invite with Holly
- [x] P5, P6, P8 highlights/tags in [Dovetail](https://dovetailapp.com/projects/e556d85e-f0cf-46ef-b5a3-27c3551cb380/data/b/50d68724-0d7c-4d25-9aa3-f6ef99673f7b)
- [x] Create MR for discrepancy between https://about.gitlab.com/handbook/engineering/ux/ux-department-workflow/#how-we-use-labels Pajamas component lifecycle labels and https://design.gitlab.com/contribute/lifecycle/ labels

↻ In progress ↻
- [ ] UX onboarding issue [#1648](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1648)
- [ ] UX transition issue [#1657](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1657) (this is on hold for now)
- [ ] UX OKR to review runner performance [#12084](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12084)
- [ ] Review runner docs [here](https://docs.google.com/document/d/1vKqCkk5jG7-cNfzhiGGArkMcd76BoeUHj1fRglcw-oI/edit)
- [ ] Review testing docs [here](https://docs.google.com/document/d/17AgZtfxyOE6kE62gvFbZTx9A69OqNSobZ77U-T_YGVY/edit?usp=sharing)

❌ Not started ❌
- [ ] Prepare something for the CI/CD UX showcase on August 18th (more details in [#47](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/47))

#### July 19 - July 23

✅ Completed ✅
- [x] Day 5 onboarding product designer issue [#2923](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2923)
- [x] Qualtrics UX jobs description survey](https://gitlab.fra1.qualtrics.com/jfe/form/SV_af36ysT2Cml9I2i)
- [x] Create [MR](https://gitlab.com/gitlab-org/gitlab-design/-/merge_requests/291) for updating group conversations link in [UX onboarding template](https://gitlab.com/gitlab-org/gitlab-design/-/tree/master/.gitlab/issue_templates)

↻ In progress ↻
- [ ] UX onboarding issue [#1648](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1648)

❌ Not started ❌
- [ ] Create MR for discrepancy between https://about.gitlab.com/handbook/engineering/ux/ux-department-workflow/#how-we-use-labels Pajamas component lifecycle labels and https://design.gitlab.com/contribute/lifecycle/ labels
- [ ] Review runner docs [here](https://docs.google.com/document/d/1vKqCkk5jG7-cNfzhiGGArkMcd76BoeUHj1fRglcw-oI/edit)
- [ ] Figure out how to share async update with team (review of onboarding process, maybe the ux scorecard process)
- [ ] Weeks 2-4 onboarding product designer issue [#2923](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2923)

